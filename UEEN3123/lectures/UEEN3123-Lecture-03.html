<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>Lecture 03: Remote Procedure Call (RPC) using RPyC</title>
</head>
<body>
<xmp>

# Lecture 03: Remote Procedure Call (RPC) using RPyC

---

## Remote Procedure Call (RPC)

* In distributed computing, a **remote procedure call (RPC)** is when a computer program causes a procedure (function / subroutine) to execute in a different address space (commonly on another computer on a shared network), which is coded as if it were a normal (local) procedure call, without the programmer explicitly coding the details for the remote interaction. 

* RPCs are a form of inter-process communication (IPC) where different processes have different address spaces.

* RPC is a request–response protocol where it is initiated by the client, which sends a request message to a known remote server to execute a specified procedure with supplied parameters. The remote server sends a response to the client, and the application continues its process.

* While the server is processing the call, the client is blocked, unless the client sends an asynchronous request to the server.

* There are many variations and subtleties in various implementations, resulting in a variety of different (incompatible) RPC protocols.

### Sequence of Events in a RPC

1. The client calls the client **stub**. The call is a **local** procedure call, with parameters pushed on to the stack in the normal way.

2. The client stub packs the parameters into a **message** and makes a system call to send the message. Packing the parameters is called **marshalling**.

3. The client's local operating system sends the message from the client machine to the server machine.

4. The local operating system on the server machine passes the incoming packets to the server **stub**.

5. The server stub unpacks the parameters from the **message**. Unpacking the parameters is called **unmarshalling**.

6. Finally, the server stub calls the server **procedure**.

7. The reply traces the same steps in the reverse direction.

<img src="images/rpc.jpg" style="max-width: 640px">

---

## RPyC

* **Remote Python Call (RPyC)** is a transparent Python library for symmetrical RPCs, clustering and distributed-computing. 

* RPyC makes use of object-proxying, a technique that employs Python’s dynamic nature, to overcome the physical boundaries between processes and computers, so that remote objects can be manipulated as if they were local.

<img src="images/rpyc.png" style="max-width: 640px">

### Uses of RPyC

* Run your tests from a central machine offering a convenient development environment, while the actual operations take place on remote ones.

* Control/administer multiple hardware or software platforms from a central point:

  * Any machine that runs Python is accessible without the need to master multiple shell-script languages (BASH, Windows batch files, etc.) and use awkward tools (awk, sed, grep, etc.)

* Access remote hardware resources transparently.

  * E.g., you have some proprietary electronic testing equipment that comes with drivers only for HPUX, but no one wants to actually use HPUX, so you just connect to the machine and use the remote `ctypes` module (or open the `/dev` file directly).

* Distributing workload among multiple machines with ease.

### Installing RPyC

* Execute this command in priviledged mode:

```
pip install RPyC
```

### A Simple RPyC Client-Server Application

#### Server program

```python
import rpyc
import math
from rpyc.utils.server import ThreadedServer


class CircleService(rpyc.Service):
    def exposed_circumference(self, radius):
        return 2 * math.pi * radius

    def exposed_area(self, radius):
        return math.pi * radius * radius


t = ThreadedServer(CircleService, port=18509)
t.start()
```

#### Client Program

* The corresponding RPyC client program

```python
import rpyc


proxy = rpyc.connect('localhost', 18509, config={'allow_public_attrs': True})

radius = float(input('Radius: '))
circumference = proxy.root.circumference(radius)
area = proxy.root.area(radius)

print('Circumference: {}'.format(circumference))
print('Area: {}'.format(area))
```

### Services

* In the abovementioned server program, you would have noticed that the `CircleService` class extends from the class `rpyc.Service`.

* RPyC is service oriented. Services provide a way to expose a well-defined set of capabilities to the other party, which makes RPyC a generic RPC platform

#### Services Boilerplate

```python
import rpyc
from rpyc.utils.server import ThreadedServer


class MyService(rpyc.Service):
    def on_connect(self, conn):
        # code that runs when a connection is created
        # (to init the service, if needed)
        pass

    def on_disconnect(self, conn):
        # code that runs after the connection has already closed
        # (to finalize the service, if needed)
        pass

    def exposed_get_answer(self): # this is an exposed method
        return 42

    exposed_the_real_answer_though = 43     # an exposed attribute

    def get_question(self):  # while this method is not exposed
        return "what is the airspeed velocity of an unladen swallow?"
```

* Apart from the special `on_connect` and `on_disconnect` methods, you are free to define the class like any other class. 

* You can choose which attributes and methods will be exposed to the other party:

  * If the name starts with `exposed_`, the attribute or method will be remotely accessible
  
  * Otherwise it is only locally accessible.
  
  * In the above boilerplate code, clients will be able to call `get_answer`, but not `get_question`.
  
#### Starting the Server

* To expose your service, you will need to start a server:

```python
# ... continuing the code snippet from above ...

if __name__ == "__main__":
    t = ThreadedServer(MyService, port=18861)
    t.start()
```

#### Accessing the Service

* To the remote party, the service is exposed as the root object of the connection, e.g., `conn.root`.

```python
>>> import rpyc
>>> c = rpyc.connect("localhost", 18861)
>>> c.root
<__main__.MyService object at 0x834e1ac>
```

* This `root` object is a reference to the service instance living in the server process.

* It can be used access and invoke exposed attributes and methods:

```python
>>> c.root.get_answer()
42
>>> c.root.the_real_answer_though
43
```

* The `get_question` method is not exposed:

```python
>>> c.root.get_question()
======= Remote traceback =======
...
  File "/home/tomer/workspace/rpyc/core/protocol.py", line 298, in sync_request
    raise obj
AttributeError: cannot access 'get_question'
```

#### Access Policy

* By default methods and attributes are only visible if they start with the `exposed_` prefix.

* This means that attributes of builtin objects such as lists or dicts are not accessible by default.

* You can, however, configure this by passing appropriate options when creating the server:

```python
from rpyc.utils.server import ThreadedServer

t = ThreadedServer(MyService, port=18861, protocol_config={
    'allow_public_attrs': True,
})
t.start()
```

#### Shared Service Instance

* In the above example, we passed the class `MyService` to the server with the effect that every incoming connection will use its own, independent `MyService` instance as root object.

* If you pass in an instance instead, all incoming connections will use this instance as their shared root object:

```python
from rpyc.utils.server import ThreadedServer

t = ThreadedServer(MyService(), port=18861)
t.start()
```

### Callbacks & Symmetry

* Passing a **callback function** means treating functions or any callable objects as first-class objects, i.e., like any other value in the language.

```python
>>> def f(x):
...     return x**2
...
>>> list(map(f, range(10)))   # f is passed as an argument to map
[0, 1, 4, 9, 16, 25, 36, 49, 64, 81]
```

#### Sample Server Program with Callback

```python
import rpyc
import math
from rpyc.utils.server import ThreadedServer


class CircleService(rpyc.Service):
    def exposed_area(self, f):
        radius = f()
        return math.pi * radius * radius


t = ThreadedServer(CircleService, port=18509)
t.start()
```

#### Sample Client Program with Callback

```python
import rpyc
from random import uniform


# Generates a random value for the radius of a circle
# from 0 to 32767
def generate_random_value():
    return uniform(0, 10)


proxy = rpyc.connect('localhost', 18509, config={'allow_public_attrs': True})

area = proxy.root.area(generate_random_value)

print('Area: {}'.format(area))
```

### Asynchrounous Operation

* **Asynchronous operation** is a key feature of RPyC.

* All the above examples you have seen are **synchronous**

  * When you call a function, you block until the result arrives.
  
* **Asynchronous** call allows you to start the request and continue, rather than waiting.

  * Instead of getting the result of the call, you get a special object known as an `AsyncResult` (also known as a _future_ or _promise_]), that will eventually hold the result.

  
* There is no guarantee on execution order for async requests!

#### Making remote function call asynchronous

* Wrap the function with `async_`

  * This creates a wrapper function that will return an `AsyncResult` instead of blocking.

  
* `AsyncResult` objects have several properties and methods that

  * `ready` - indicates whether or not the result arrived

  * `error` - indicates whether the result is a value or an exception

  * `expired` - indicates whether the `AsyncResult` object is expired (its time-to-wait has elapsed before the result has arrived). Unless set by set_expiry, the object will never expire

  * `value` - the value contained in the `AsyncResult`. If the value has not yet arrived, accessing this property will block. If the result is an exception, accessing this property will raise it. If the object has expired, an exception will be raised. Otherwise, the value is returned

  * `wait()` - wait for the result to arrive, or until the object is expired

  * `add_callback(func)` - adds a callback to be invoked when the value arrives

  * `set_expiry(seconds)` - sets the expiry time of the AsyncResult. By default, no expiry time is set

#### Sample Server Program

```python
import rpyc
import math
import time
from rpyc.utils.server import ThreadedServer


class CircleService(rpyc.Service):
    def exposed_circumference(self, radius):
        time.sleep(5)       # pause for 5 seconds
        return 2 * math.pi * radius

    def exposed_area(self, radius):
        time.sleep(10)  # pause for 10 seconds
        return math.pi * radius * radius


t = ThreadedServer(CircleService, port=18509)
t.start()
```

#### Sample Client Program

```python
import rpyc


def show_circumference(res):
    print('Circumference: {}'.format(res.value))


def show_area(res):
    print('Area: {}'.format(res.value))


proxy = rpyc.connect('localhost', 18509, config={'allow_public_attrs': True})

radius = float(input('Radius: '))

# Wrap the remote function with async_(), which turns the call asynchronous
async_circumference = rpyc.async_(proxy.root.circumference)
async_area = rpyc.async_(proxy.root.area)

# Call the asynchronous remote functions & add_callback to display results when ready
res_circumference = async_circumference(radius)
res_circumference.add_callback(show_circumference)
res_area = async_area(radius)
res_area.add_callback(show_area)

print('Awaiting results from service...')

# Wait for the results
res_circumference.wait()
res_area.wait()
```

---

</xmp> 
<script type="text/javascript" src="https://cdn.rawgit.com/Naereen/StrapDown.js/master/strapdown.min.js?mathjax=y&src=example5&theme=united"></script>
</body>
</html>
