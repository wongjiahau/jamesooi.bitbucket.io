<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>Lecture 04: Advanced Sockets - Unicast, Broadcast &amp; Multicast</title>
</head>
<body>
<xmp>

# Lecture 04: Advanced Sockets

---

## Unicast, Broadcast & Multicast

<img src="images/unimultibroad.png" style="max-width: 640px">

### Unicast

* Data is sent from one computer to another computer.

* Only one sender and one receiver involved in the data communications.

* Most network applications we use are unicast applications:

  * Web applications.
  
  * File transfer (FTP, TFTP).
  
  * Email.
  
* Unicast addresses:

  * Represent a single network interface.
  
  * A unicast packet will be sent to a specific device, not to a group of devices on the network.
  
  * Interfaces connected to a network are assigned with one or more unicast IP address, e.g.:
  
    * `192.168.6.66` (IPv4)
	
	* `2001:e68:5444:509:face:b00c:ace:ace` (IPv6)

### Broadcast

* Data is sent from one computer once and a copy of that data will be forwarded to all the devices on the network.

* There is only one sender and the data is sent only once. However the data is delivered to all connected devices.

* **Switches** (Layer 2) by design will forward the broadcast traffic to all devices within a broadcast domain while **Routers** (Layer 3) by design will drop the broadcast traffic.

  * Routers will not allow a broadcast from one subnet to cross the Router and reach another subnet.
  
  
* Broadcast applications:

  * ARP Request message.
  
  * DHCP DISCOVER message.
  
  * RIP (v1) routing information.
  
* Broadcast addresses:

  * Broadcast MAC address: `FF:FF:FF:FF:FF:FF`
  
  * Broadcast IPv4 address: Last IP address of the subnet, e.g. for the subnet `192.168.6.0/24`, the broadcast address is `192.168.6.255`.
  
  * There is **NO** broadcast IPv6 address. IPv6 does NOT support broadcasting at Layer 3.
  
### Multicast

* A type of communication where multicast traffic is addressed for a group of devices on the network.

* IP multicast traffic are sent to a group and only members of that group receive and process the multicast traffic.

* Devices that would like to receive a particular multicast traffic must join the multicast group using the multicast address of that group to receive the traffic.

* The sender transmit only one copy of data and it is delivered to all devices that joined the multicast group (As opposed to being delivered to all devices in the network as in Broadcast).

* Multicast applications:

  * IPTV
  
  * Routing information (RIP (v2), OSPF, EIGRP)
  

* Multicast addresses:

  * `224.0.0.3` (IPv4)
  
  * `ffx5::ace` (IPv6)

### Unicast Programs

* All the networked programs that we have learnt so far are unicast applications.

* In unicast, we can choose to use, at the Transport Layer (Layer 4), either TCP (using stream socket) or UDP (using datagram socket) protocols, depending on application requirements.

### Multicast Programs

* In multicast, we can only use UDP (datagram socket) at the Transport Layer. TCP is not supported.

#### Motivation

* Normally, there is only one peer that we want to send a UDP datagram to.

* What if we want to send the same data to more than one peer?

  * Using **unicast**, we must do multiple sends. This resuls in multiple copies of the same packets in transit in our network.
  
  * Using **multicast**, we send only one copy of each datagram, the routers are responsible to distribute them to the intended receipients.
  
#### Advantages

* When clients need to look for services, instead of sending out unicast requests by cycling through a range of addresses where the service might be, the client can send out a single multicast request.

* Instances of the service are listening for such multicasts, and each instance responds by sending its unicast address back to the client

  * This completes the service-location process.
  

* There is a substantial saving in network bandwidth and therefore in network usage costs as well as saving in time to propogate the datagrams when using multicasting, especially with audio and video streaming applications.

* Compared to using broadcast, there is the unwanted load incurred by hosts which are not listening.

  * The broadcast datagram is still received by the network interface and propagated upwards through the various layers of the protocol stack, even to devices not interested with the traffic.
  
#### Multicast Server Program

* For multicast server programs, create a **datagram socket**.

```python
s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
```

* Set the socket option for `IP_MULTICAST_TTL` (IP Multicast Time to Live) of `IPPROTO_IP ` (IP layer).

  * Specifies the TTL to the value you desire (from `0` to `255`).
  
  * A value of `1` means the multicast datagrams will not be forwarded beyond the local subnet.
  
  * In other words, it specifies how many routers the datagram will be forwarded before being dropped.

```python
s.setsockopt(socket.IPPROTO_IP, socket.IP_MULTICAST_TTL, 1)
```

* Use the `sendto` method to send the multicast datagram to the intended multicast group (specified by the multicast IP address and port number):

```python
MULTICAST_GROUP = '224.0.0.3'
PORT = 8509

s.sendto(data.encode(), (MULTICAST_GROUP, PORT))
```

##### Example

```python
import socket
import time
from datetime import datetime

MULTICAST_GROUP = '224.0.0.3'
PORT = 8509

s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
s.setsockopt(socket.IPPROTO_IP, socket.IP_MULTICAST_TTL, 1)

while True:
    response = str(datetime.now())

    s.sendto(response.encode(), (MULTICAST_GROUP, PORT))
    print('[+] Sent data to {}'.format(MULTICAST_GROUP))

    time.sleep(2)
```

#### Multicast Client Program

* Just like multicast server programs, create a **datagram socket** for the multicast client program to receive multicast datagrams.

```python
s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
```

* Set the following socket options:

  * `SO_REUSEADDR` (Reuse socket address) of `SOL_SOCKET ` (socket layer).
  
    * Set to `1` (`True`) to tell the kernel to reuse a local socket in `TIME_WAIT` state, without waiting for its natural timeout to expire. 
	
	* This prevents an address already in use error if multiple client programs run on the same machine.
  
  * `IP_ADD_MEMBERSHIP` (Join a multicast group) of `IPPROTO_IP ` (IP layer).
  
    * Argument is an `ip_mreq` structure:
	
	```c
	struct ip_mreq {
      struct in_addr imr_multiaddr; /* IP multicast group address */
      struct in_addr imr_address;   /* IP address of local interface */
    };
	```
	
	* `imr_multiaddr` contains the address of the multicast group.
	
	* `imr_address` is the address of the local interface with which the system should join the
       multicast group.
	
	* In Python, we use the `struct.pack` method to pack the values into an `ip_mreq` struct:
	
	  * Syntax:
	  
	  ```python
	  struct.pack(format, v1, v2, ...)
	  ```
	
	  * Usage:
	
	  ```python
	  struct.pack("=4sl", socket.inet_aton(MULTICAST_GROUP), socket.INADDR_ANY)
	  ```
	
	  * The `format` value `"=4sl"`:

	    * `=` means native byte order, standard sizes, and no alignment.
		
		* `4s` means four-letter string (four char joined together into a bytestring)
		
		* `l` means signed long, in this case a four-byte `int`.

* Use the `recv` method to receive data from the multicast group:

```python
s.recv(1024)
```

##### Example

```python
import socket
import struct

MULTICAST_GROUP = '224.0.0.3'
PORT = 8509

s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
mreq = struct.pack("=4sl", socket.inet_aton(MULTICAST_GROUP), socket.INADDR_ANY)
s.setsockopt(socket.IPPROTO_IP, socket.IP_ADD_MEMBERSHIP, mreq)
s.bind(('', PORT))

while True:
    data = s.recv(1024)
    print('Server Time: {}'.format(data.decode()))
```

### Broadcast Programs

#### Broadcast Sender

* For broadcast sender programs, create a **datagram socket**.

```python
s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
```

* Set the socket option for `SO_BROADCAST` (broadcast socket) of `SOL_SOCKET ` (socket layer) to `1` (`True`) to enable broadcast.

```python
s.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
```

* Use the `sendto` method to send the data to `('<broadcast>', PORT)`:

```python
s.sendto(response.encode(), ('<broadcast>', PORT))
```

##### Example

```python
import socket
import time
from datetime import datetime

PORT = 9999

s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
s.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)

while True:
    response = str(datetime.now())

    s.sendto(response.encode(), ('<broadcast>', PORT))
    print('[+] Broadcasted data to UDP port {}'.format(PORT))

    time.sleep(2)
```

#### Broadcast Receiver

* For broadcast receiver programs, create a **datagram socket**.

```python
s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
```

* Set the socket option for `SO_BROADCAST` (broadcast socket) of `SOL_SOCKET ` (socket layer) to `1` (`True`) to enable broadcast.

* Also set the socket option `SO_REUSEADDR` to `1`.

```python
s.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
s.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
```

* Use the `recvfrom` method to receive the broadcast data:

```python
data, addr = s.recvfrom(1024)
```

##### Example

```python
import socket

PORT = 9999

s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
s.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
s.bind(('', PORT))

while True:
    data, addr = s.recvfrom(1024)

    print('Received from {}; Server Time: {}'.format(addr, data.decode()))
```
---

---

</xmp> 
<script type="text/javascript" src="https://cdn.rawgit.com/Naereen/StrapDown.js/master/strapdown.min.js?mathjax=y&src=example5&theme=united"></script>
</body>
</html>
