<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>Lecture 02: TCP &amp; UDP Client-Server Applications</title>
</head>
<body>
<xmp>

# Lecture 02: TCP & UDP Client-Server Applications

---

## Transmission Control Protocol (TCP) vs User Datagram Protocol (UDP)

| TCP | UDP |
| ----- | ----- |
| connection-oriented protocol | connectionless protocol |
| suited for applications that require high reliability, and transmission time is relatively less critical | suitable for applications that need fast, efficient transmission such as games, & real time applications such as video audio & video streaming (IPTV, IP telephony, video calls) |
| rearranges data packets in the order specified | no inherent order as all packets are independent of each other. If ordering is required, it has to be managed by the application layer |
| absolute guarantee that the data transferred remains intact and arrives in the same order in which it was sent | no guarantee that the messages or packets sent would reach at all |
| speed for TCP is slower than UDP | faster because error recovery is not attempted. It is a "best effort" protocol |
| data is read as a byte stream, no distinguishing indications | packets are sent individually and are checked for integrity only if they arrive |

---

## Stream Socket & Datagram Socket

### Stream Socket

* Use **TCP** for data transmission. 

* A dedicated point-to-point channel between a client and server.

* Lossless and reliable. 

* Sent and received in the same order.

* In Python, uses the socket type `SOCK_STREAM`:

```python
socket.socket(socket.AF_INET6, socket.SOCK_STREAM)
```

* Uses the method `recv` to receive data.

  * Syntax:
  
  ```python
  socket.recv(bufsize[, flags])
  ```
  
  * Receive data from the socket. The return value is a bytes object representing the data received. The maximum amount of data to be received at once is specified by `bufsize`.

  
* Uses the method `send` / `sendall` to send data.

  * Syntax for `send`:
  
  ```python
  socket.send(bytes[, flags])
  ```
  
  * Send data to the socket. The socket must be connected to a remote socket. Returns the number of bytes sent.
  
  * Syntax for `sendall`:
  
  ```python
  socket.sendall(bytes[, flags])
  ```
  
  * Send data to the socket. The socket must be connected to a remote socket. Unlike `send()`, this method continues to send data from bytes until either all data has been sent or an error occurs. None is returned on success. On error, an exception is raised, and there is no way to determine how much data, if any, was successfully sent.


### Datagram Socket

* Use **UDP** for data transmission. 

* No dedicated point-to-point channel between a client and server.

* May lose data and not 100% reliable. 

* Data may not received in the same order as sent.

* In Python, uses the socket type `SOCK_DGRAM`

```python
socket.socket(socket.AF_INET6, socket.SOCK_DGRAM)
```

* Uses the method `recvfrom` to read data.

  * Syntax:
  
  ```python
  socket.recvfrom(bufsize[, flags])
  ```
  
  * Receive data from the socket. The return value is a pair `(bytes, address)` where `bytes` is a bytes object representing the data received and `address` is the address of the socket sending the data.

  
* Uses the method `sendto` to write data.

  * Syntax:
  
  ```python
  socket.sendto(bytes, address)
  socket.sendto(bytes, flags, address)
  ```
  
  * Send data to the socket. The socket should not be connected to a remote socket, since the destination socket is specified by address.

---

## A UDP Time Server Application

### Server Program

```python
import socket
from datetime import datetime

HOST = ''           # Symbolic name meaning all available interfaces
PORT = 8509         # The UDP port this server is bound to

# Create a socket object, bind it to (HOST, PORT)
s = socket.socket(socket.AF_INET6, socket.SOCK_DGRAM)  # SOCK_DGRAM is used for UDP Datagram Socket
s.bind((HOST, PORT))

while True:
    # Receive data from client
    data, addr = s.recvfrom(1024)
    if not data:
        break

    print('[+] Received data from {}'.format(addr))
    response = str(datetime.now())

    # Send
    s.sendto(response.encode(), addr)
    print('[+] Sent data to {}'.format(addr))
```

### Client Program

```python
import socket
import time

HOST = '::1'           # IPv6 address of the server
PORT = 8509         # The UDP port number of the server

# Create a socket object
s = socket.socket(socket.AF_INET6, socket.SOCK_DGRAM)

while True:
    message = 'dummy'

    # Send message to server
    s.sendto(message.encode(), (HOST, PORT))

    # Receive response from server
    data, addr = s.recvfrom(1024)

    # Print output
    print('Server Time: {}'.format(data.decode()))

    time.sleep(3)
```

---

## A more flexible implementation

* Allow IP address and port numbers to be specified instead of being hardcoded.

* Uses command-line arguments

### Command-line Arguments

* The Python `sys` module provides access to any command-line arguments via the `sys.argv`.

* This serves two purposes:

  * `sys.argv` is the list of command-line arguments.

  * `len(sys.argv)` is the number of command-line arguments.

* Here `sys.argv[0]` is the program ie. script name.

### Example

#### Server Program
```python
import sys
import socket
import threading
import math
import json

class ClientThread(threading.Thread):
    def __init__(self, conn, addr):
        threading.Thread.__init__(self)
        self.socket = conn
        self.addr = addr
        print('[+] New thread started for client {0}'.format(addr))

    def run(self):
        while True:
            # Receive radius from client, decode the bytes to string
            # & convert it to float
            radius = float(self.socket.recv(1024).decode())
            print('[*] Received data from client {0}, value {1}'.format(self.addr, radius))

            # Calculate the circumference & area
            circum = 2 * math.pi * radius
            area = math.pi * radius * radius

            # Wrap the results into a dict
            response = {
                'circum': circum,
                'area': area,
            }

            # Serialize dict into JSON, encode into bytes & send to client
            self.socket.sendall(json.dumps(response).encode())
            print('[*] Sent data to client {0}'.format(self.addr))


if len(sys.argv) != 2:
    print('[*** ERROR ***] Invalid number of arguments')
    quit()

HOST = ''                  # Symbolic name meaning all available interfaces
PORT = int(sys.argv[1])    # The TCP port number this server is bound to

# Create a socket object, bind it to (HOST, PORT) &
# listen for incoming connection
s = socket.socket(socket.AF_INET6, socket.SOCK_STREAM)  # use AF_INET for IPv4
s.bind((HOST, PORT))
s.listen()


# Infinite loop, each iteration handles a connection from a client
while True:
    # Accepts a connection
    conn, addr = s.accept()

    # Start thread
    t = ClientThread(conn, addr)
    t.start()
```

#### Client Program

```python
import sys
import socket
import json

if len(sys.argv) != 3:
    print('[*** ERROR ***] Invalid number of arguments')
    quit()

HOST = sys.argv[1]        # IPv6 address of the server
PORT = int(sys.argv[2])   # The TCP port number of the server

# Create a socket object, connect to (HOST, PORT)
s = socket.socket(socket.AF_INET6, socket.SOCK_STREAM)  # use AF_INET for IPv4
s.connect((HOST, PORT))

print('Circle calculator')

# Get the radius from the user interactively
radius = input('Radius (0 to Quit): ')

while radius != '0':
    # Encode the input into bytes & send to server
    s.sendall(radius.encode())

    # Receive the response from the server & convert the bytes (JSON string)
    # into dict
    response = json.loads(s.recv(1024))

    # Print the result
    print('Circumference: {}'.format(response['circum']))
    print('Area: {}'.format(response['area']))

    # Get the radius from the user interactively
    radius = input('Radius (0 to Quit): ')

# Close the socket
s.close()
```

---

## Socket Options

* In Python, socket options that control specific behaviors of network sockets are accessed through the Python socket methods `getsockopt()` and `setsockopt()`.

### getsockopt()

#### Syntax

```python
socket.getsockopt(level, optname[, buflen])
```

* Returns the value of the given socket option. The needed symbolic constants (`SO_*`, etc.) are defined in this module.

* `level` specifies the level of the socket. For our stream-based and datagram-based socket, we use `socket.SOL_SOCKET`, meaning the _socket-level options_.

* `optname` specifies the name of the socket options.

* If `buflen` is absent, an integer option is assumed and its integer value is returned by the function. If `buflen` is present, it specifies the maximum length of the buffer used to receive the option in, and this buffer is returned as a bytes object.

* It is up to the caller to decode the contents of the buffer.

#### Example

```python
value = socket.getsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST)
```

### setsockopt()

#### Syntax

```python
socket.setsockopt(level, optname, value: int)

socket.setsockopt(level, optname, value: buffer)
```

* Sets the value of the given socket option. The needed symbolic constants are defined in the socket module (`SO_*`, etc.).

* `level` specifies the level of the socket.

* `optname` specifies the name of the socket options.

* The value can be an integer, or a bytes-like object representing a buffer. In the latter case it is up to the caller to ensure that the bytestring contains the proper bits.

#### Example

```python
value = 1
socket.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, value)
```

### Commonly-used Socket Options

#### TCP_NODELAY

* Used to disable Nagle's Algorithm to improve TCP/IP networks and decrease the number of packets by waiting until an acknowledgment of previous sent data is received to send the accumulated packets.

* Valid for client sockets (TCP)

* `level`: `SOL_TCP`

* Example:

  ```python
  s.setsockopt(socket.SOL_TCP, socket.TCP_NODELAY, 1)
  ```

#### SO_TIMEOUT

* In Python, a special method `settimeout` is provided. Use this method instead of the `setsockopt` method.

* Syntax:

  ```python
  socket.settimeout(value)
  ```

* Sets a timeout on blocking socket operations.

* The `value` argument can be a non-negative floating point number expressing seconds, or `None`.

* If a non-zero value is given, subsequent socket operations will raise a timeout exception if the timeout period value has elapsed before the operation has completed.

* If zero is given, the socket is put in non-blocking mode.

* If `None` is given, the socket is put in blocking mode.

* Valid for all sockets.

* Example:

  ```python
  s.settimeout(5)
  ```

#### SO_BROADCAST

* Allows broadcast UDP packets to be sent and received.

* Valid for UDP sockets.

* `level`: `SOL_SOCKET`

* Example:

  ```python
  s.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
  ```

#### SO_DONTROUTE

* Only be willing to send packets that are addressed to hosts on subnets to which this computer is connected directly.

* `level`: `SOL_SOCKET`

* Example:

  ```python
  s.setsockopt(socket.SOL_SOCKET, socket.SO_DONTROUTE, 1)
  ```

#### SO_TYPE

* When passed to `getsockopt`, this returns to you regardless of whether a socket is of type `SOCK_DGRAM` and can be used for UDP, or it is of type `SOCK_STREAM` and instead supports the semantics of TCP.

* `level`: `SOL_SOCKET`

* Example:

  ```python
  stype = s.getsockopt(socket.SO_TYPE)
  ```

#### SO_KEEPALIVE

* Periodically test if connection still alive.

* Warning though, it generates extra network traffic, which can have an impact on routers and firewalls.

* `level`: `SOL_SOCKET`

* Example:

  ```python
  s.setsockopt(socket.SOL_SOCKET, socket.SO_KEEPALIVE, 1)
  ```

#### SO_RCVBUF

* Sets the receive buffer size, values should be powers of 2.

* `level`: `SOL_SOCKET`

* Example:

  ```python
  s.setsockopt(socket.SOL_SOCKET, socket.SO_RCVBUF, 8192)
  ```
  
#### SO_SNDBUF 

* Sets the send buffer size, values should be powers of 2.

* `level`: `SOL_SOCKET`

* Example:

  ```python
  s.setsockopt(socket.SOL_SOCKET, socket.SO_SNDBUF, 4096)
  ```

## Code difference between UDP server and TCP server
### Code for UDP server
```python
import socket
import json

HOST = ''
PORT = 8509

# Differnce 1: use SOCK_DGRAM 
s = socket.socket(socket.AF_INET6, socket.SOCK_DGRAM)
s.bind((HOST, PORT))

# Difference 2: No need s.listen()

while True:
    #Difference 3:
    # - use data,addr
    # - use s.recvFrom(1024)
    data, addr = s.recvfrom(1024)

    # Difference 4: No need receive data again

    print(f"[+] Received data from {addr}")
    response = json.loads(data)
    result = float(response["current"]) * float(response["resistance"])
    print(f"Sending response . . . ")

    ## Difference 5: use s.sendto() 
    s.sendto(json.dumps({"voltage": result}).encode(), addr)
```
## Code for TCP server
```python
import socket
import json

HOST = ''
PORT = 8509

# Difference 1: use SOCK_STREAM
s = socket.socket(socket.AF_INET6, socket.SOCK_STREAM)
s.bind((HOST, PORT))

# Difference 2: Need to call s.listen()
s.listen()

while True:
    # Difference 3: 
    # - use conn,addr
    # - use s.accept()
    conn, addr = s.accept()

    # Difference 4: Need to receive data using connection
    data = conn.recv(1024).decode()

    print(f"[+] Received data {data} from {addr}")
    response = json.loads(data)
    result = float(response["current"]) * float(response["resistance"])
    print(f"Sending response . . . ")

    ## Difference 5: use s.sendall()
    conn.sendall(json.dumps({"voltage": result}).encode())
```
---

</xmp> 
<script type="text/javascript" src="https://cdn.rawgit.com/Naereen/StrapDown.js/master/strapdown.min.js?mathjax=y&src=example5&theme=united"></script>
</body>
</html>
