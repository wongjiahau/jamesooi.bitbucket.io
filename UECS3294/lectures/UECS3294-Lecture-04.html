<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>Lecture 04: Validation</title>
</head>
<body>
<xmp>

# Lecture 04: Validation

---

## Introduction

* Laravel provides several different approaches to validate your application's incoming data.

* By default, Laravel's base controller class uses a `ValidatesRequests` trait which provides a convenient method to validate incoming HTTP request with a variety of powerful validation rules.

---

## Implementing Validation

* Using an example of validating a form and displaying the error messages back to the user, let's assume we have the following routes defined in our `routes/web.php` file:

```php
Route::get('post/create', 'PostController@create');

Route::post('post', 'PostController@store');
```

* A simple controller that handles these routes with the `store` method currently empty is as shown below:

```php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PostController extends Controller
{
    /**
     * Show the form to create a new blog post.
     *
     * @return Response
     */
    public function create()
    {
        return view('post.create');
    }

    /**
     * Store a new blog post.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        // Validate and store the blog post...
    }
}
```

### Building The Validation Logic

* We will need to fill in our `store` method with the logic to validate the new blog post.

* To do this, we will use the `validate` method provided by the `Illuminate\Http\Request` object.

* If the validation rules pass, your code will keep executing normally; however, if validation fails, an exception will be thrown and the proper error response will automatically be sent back to the user.

* In the case of a traditional HTTP request, a redirect response will be generated, while a JSON response will be sent for AJAX requests.

```php
/**
 * Store a new blog post.
 *
 * @param  Request  $request
 * @return Response
 */
public function store(Request $request)
{
    $validatedData = $request->validate([
        'title' => 'required|unique:posts|max:255',
        'body' => 'required',
    ]);

    // The blog post is valid...
}
```

* In the above code, we pass the desired validation rules into the `validate` method.

* If the validation fails, the proper response will automatically be generated. If the validation passes, our controller will continue executing normally.

#### Stopping On First Validation Failure

* Sometimes you may wish to stop running validation rules on an attribute after the first validation failure. To do so, assign the `bail` rule to the attribute:

```php
$request->validate([
    'title' => 'bail|required|unique:posts|max:255',
    'body' => 'required',
]);
```

* In this example, if the `unique` rule on the title attribute fails, the `max` rule will not be checked.

* **NOTE:** Rules will be validated in the order they are assigned.

#### Nested Attributes

If your HTTP request contains _nested_ parameters, you may specify them in your validation rules using _dot_ syntax:

```php
$request->validate([
    'title' => 'required|unique:posts|max:255',
    'author.name' => 'required',
    'author.description' => 'required',
]);
```

### Displaying The Validation Errors

* Laravel will automatically redirect the user back to their previous location if validation fails.

* In addition, all of the validation errors will automatically be flashed to the session.

* We do not have to explicitly bind the error messages to the view in our GET route. This is because Laravel will check for errors in the session data, and automatically bind them to the view if they are available.

* The `$errors` variable will be an instance of `Illuminate\Support\MessageBag` and is bound to the view by the `Illuminate\View\Middleware\ShareErrorsFromSession` middleware, which is provided by the `web` middleware group.

* When this middleware is applied, an `$errors` variable will always be available in your views, allowing you to conveniently assume the `$errors` variable is always defined and can be safely used.

* So, in our example, the user will be redirected to our controller's `create` method when validation fails, allowing us to display the error messages in the view:

```php
<!-- /resources/views/post/create.blade.php -->

<h1>Create Post</h1>

@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<!-- Create Post Form -->
```

### Optional Fields

* By default, Laravel includes the `TrimStrings` and `ConvertEmptyStringsToNull` middleware in your application's global middleware stack.

* These middleware are listed in the stack by the `App\Http\Kernel` class.

* Because of this, you will often need to mark your optional request fields as `nullable` if you do not want the validator to consider `null` values as invalid:

```php
$request->validate([
	'title' => 'required|unique:posts|max:255',
	'body' => 'required',
	'publish_at' => 'nullable|date',
]);
```

* In the above example, we are specifying that the `publish_at` field may be either `null` or a valid date representation.

* If the `nullable` modifier is not added to the rule definition, the validator would consider `null` an invalid date.


### AJAX Requests & Validation

* In the above example, we used a traditional form to send data to the application.

* However, many applications use AJAX requests.

* When using the `validate` method during an AJAX request, Laravel will not generate a redirect response.

* Instead, Laravel generates a JSON response containing all of the validation errors. This JSON response will be sent with a 422 HTTP status code.

---

## Form Request Validation

### Creating Form Requests

* For more complex validation scenarios, you may wish to create a _form request_.

* Form requests are custom request classes that contain validation logic.

* To create a form request class, use the `make:request` Artisan CLI command:

```
php artisan make:request StoreBlogPost
```

* The generated class will be placed in the `app/Http/Requests` directory.

* If this directory does not exist, it will be created when you run the `make:request` command. 

* Let's add a few validation rules to the rules method:

```php
/**
 * Get the validation rules that apply to the request.
 *
 * @return array
 */
public function rules()
{
    return [
        'title' => 'required|unique:posts|max:255',
        'body' => 'required',
    ];
}
```

* All you need to do is **type-hint** the request on your controller method.

* The incoming form request is validated before the controller method is called, meaning you do not need to clutter your controller with any validation logic:


```php
/**
 * Store the incoming blog post.
 *
 * @param  StoreBlogPost  $request
 * @return Response
 */
public function store(StoreBlogPost $request)
{
    // The incoming request is valid...
}
```

* If validation fails, a redirect response will be generated to send the user back to their previous location.

* The errors will also be flashed to the session so they are available for display.

* If the request was an AJAX request, a HTTP response with a 422 status code will be returned to the user including a JSON representation of the validation errors.

### Adding After Hooks To Form Requests

* If you would like to add an _after_ hook to a form request, you may use the `withValidator` method.

* This method receives the fully constructed validator, allowing you to call any of its methods before the validation rules are actually evaluated:

```php
/**
 * Configure the validator instance.
 *
 * @param  \Illuminate\Validation\Validator  $validator
 * @return void
 */
public function withValidator($validator)
{
    $validator->after(function ($validator) {
        if ($this->somethingElseIsInvalid()) {
            $validator->errors()->add('field', 'Something is wrong with this field!');
        }
    });
}
```

---

## Customizing The Error Messages

* You may customize the error messages used by the form request by overriding the `messages` method.

* This method should return an array of attribute/rule pairs and their corresponding error messages:

```php
/**
 * Get the error messages for the defined validation rules.
 *
 * @return array
 */
public function messages()
{
    return [
        'title.required' => 'A title is required',
        'body.required'  => 'A message is required',
    ];
}
```

---

## Manually Creating Validators

* If you do not want to use the `validate` method on the request, you may create a validator instance manually using the `Validator` facade.

* The `make` method on the facade generates a new validator instance:

```php
namespace App\Http\Controllers;

use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PostController extends Controller
{
    /**
     * Store a new blog post.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'required|unique:posts|max:255',
            'body' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect('post/create')
				->withErrors($validator)
                ->withInput();
        }

        // Store the blog post...
    }
}
```

* The first argument passed to the `make` method is the data under validation.

* The second argument is the validation rules that should be applied to the data.

* After checking if the request validation failed, you may use the `withErrors` method to flash the error messages to the session.

* When using this method, the $errors variable will automatically be shared with your views after redirection, allowing you to easily display them back to the user.

* The withErrors method accepts a validator, a `MessageBag`, or a PHP array.


### Automatic Redirection

* If you would like to create a validator instance manually but still take advantage of the automatic redirection offered by the requests's `validate` method, you may call the `validate` method on an existing validator instance.

* If validation fails, the user will automatically be redirected or, in the case of an AJAX request, a JSON response will be returned:

```php
Validator::make($request->all(), [
    'title' => 'required|unique:posts|max:255',
    'body' => 'required',
])->validate();
```

### Named Error Bags

* If you have multiple forms on a single page, you may wish to name the `MessageBag` of errors, allowing you to retrieve the error messages for a specific form.

* Pass a name as the second argument to `withErrors`:

```php
return redirect('register')
	->withErrors($validator, 'login');
```

* You may then access the named `MessageBag` instance from the `$errors` variable:

```php
{{ $errors->login->first('email') }}
```

### After Validation Hook

* The validator also allows you to attach callbacks to be run after validation is completed. 

* This allows you to easily perform further validation and even add more error messages to the message collection.

* To get started, use the `after` method on a validator instance:

```php
$validator = Validator::make(...);

$validator->after(function ($validator) {
    if ($this->somethingElseIsInvalid()) {
        $validator->errors()->add('field', 'Something is wrong with this field!');
    }
});

if ($validator->fails()) {
    //
}
```

---

## Working With Error Messages

* After calling the `errors` method on a `Validator` instance, you will receive an `Illuminate\Support\MessageBag` instance, which has a variety of convenient methods for working with error messages.

* The `$errors` variable that is automatically made available to all views is also an instance of the `MessageBag` class.

### Retrieving The First Error Message For A Field

* To retrieve the first error message for a given field, use the `first` method:

```php
$errors = $validator->errors();

echo $errors->first('email');
```

### Retrieving All Error Messages For A Field

* If you need to retrieve an array of all the messages for a given field, use the `get` method:

```php
foreach ($errors->get('email') as $message) {
    //
}
```

* If you are validating an array form field, you may retrieve all of the messages for each of the array elements using the `*` character:

```php
foreach ($errors->get('attachments.*') as $message) {
    //
}
```

### Retrieving All Error Messages For All Fields

* To retrieve an array of all messages for all fields, use the `all` method:

```php
foreach ($errors->all() as $message) {
    //
}
```

### Determining If Messages Exist For A Field

* The `has` method may be used to determine if any error messages exist for a given field:

```php
if ($errors->has('email')) {
    //
}
```

### Custom Error Messages

* If needed, you may use custom error messages for validation instead of the defaults.

* There are several ways to specify custom messages.

* First, you may pass the custom messages as the third argument to the `Validator::make` method:

```php
$messages = [
    'required' => 'The :attribute field is required.',
];

$validator = Validator::make($input, $rules, $messages);
```

* In this example, the `:attribute` placeholder will be replaced by the actual name of the field under validation.

* You may also utilize other placeholders in validation messages, such as:

```php
$messages = [
    'same'    => 'The :attribute and :other must match.',
    'size'    => 'The :attribute must be exactly :size.',
    'between' => 'The :attribute value :input is not between :min - :max.',
    'in'      => 'The :attribute must be one of the following types: :values',
];
```

### Specifying A Custom Message For A Given Attribute

* Sometimes you may wish to specify a custom error messages only for a specific field.

* You may do so using **dot** notation. Specify the attribute's name first, followed by the rule:

```php
$messages = [
    'email.required' => 'We need to know your e-mail address!',
];
```
---

## Available Validation Rules

* Please refer to https://laravel.com/docs/5.5/validation#available-validation-rules

### Conditionally Adding Rules

#### Validating When Present

* In some situations, you may wish to run validation checks against a field only if that field is present in the input array.

* To quickly accomplish this, add the `sometimes` rule to your rule list:

```php
$v = Validator::make($data, [
    'email' => 'sometimes|required|email',
]);
```

* In the example above, the `email` field will only be validated if it is present in the `$data` array.

#### Complex Conditional Validation

*Sometimes you may wish to add validation rules based on more complex conditional logic.

* E.g., you may wish to `require` a given field only if another field has a  value greater than `100`. Or, you may need two fields to have a given value only when another field is present.

* Adding these validation rules doesn't have to be a pain. First, create a `Validator` instance with your static rules that never change:

```php
$v = Validator::make($data, [
    'email' => 'required|email',
    'games' => 'required|numeric',
]);
```

* Let's assume our web application is for game collectors.

* If a game collector registers with our application and they own more than 100 games, we want them to explain why they own so many games, perhaps they run a game resale shop, or maybe they just enjoy collecting.

* To conditionally add this requirement, we can use the `sometimes` method on the `Validator` instance.

```php
$v->sometimes('reason', 'required|max:500', function ($input) {
    return $input->games >= 100;
});
```

* The first argument passed to the `sometimes` method is the name of the field we are conditionally validating.

* The second argument is the rules we want to add.

* If the `Closure` passed as the third argument returns `true`, the rules will be added. This method makes it a breeze to build complex conditional validations.

* You may even add conditional validations for several fields at once:

```php
$v->sometimes(['reason', 'cost'], 'required', function ($input) {
    return $input->games >= 100;
});
```
---
## Custom Validation Rules

### Using Rule Objects

* Laravel provides a variety of helpful validation rules; however, you may wish to specify some of your own.

* One method of registering custom validation rules is using _rule objects_. 

* To generate a new rule object, you may use the `make:rule` Artisan  CLI command.

* Let's use this command to generate a rule that verifies a string is uppercase.

* Laravel will place the new rule in the `app/Rules` directory:

```
php artisan make:rule Uppercase
```

* Once the rule has been created, we are ready to define its behavior.

* A rule object contains two methods: `passes` and `message`.

* The `passes` method receives the attribute value and name, and should return `true` or `false` depending on whether the attribute value is valid or not.

* The `message` method should return the validation error message that should be used when validation fails

```php
namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class Uppercase implements Rule
{
    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        return strtoupper($value) === $value;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'The :attribute must be uppercase.';
    }
}
```

* Once the rule has been defined, you may attach it to a validator by passing an instance of the rule object with your other validation rules:

```php
use App\Rules\Uppercase;

$request->validate([
    'name' => ['required', new Uppercase],
]);
```

---
###### The contents in this page are generally based on Laravel's Documentation at https://laravel.com/docs/5.5
</xmp>
<script type="text/javascript" src="https://cdn.rawgit.com/Naereen/StrapDown.js/master/strapdown.min.js?mathjax=y&src=example5&theme=united"></script>
</body>
</html>
