<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>Lecture 03: Database &amp; Eloquent ORM</title>
</head>
<body>
<xmp>

# Lecture 03: Database & Eloquent ORM

---

## Introduction

* Laravel makes interacting with databases simple

* We can access databases in Laravel using:

  * Raw SQL

  * Query builder

  * Eloquent Object-Relational Mapping (ORM)

* Currently, Laravel supports four databases:

  * MySQL

  * PostgreSQL

  * SQLite

  * SQL Server

* Support for Oracle is possible with third-party packages.

* In our class, we will only focus on MySQL databases. Tutorials & guides for using other databases can be found in Laravel documentation.

## Eloquent ORM

* Eloquent ORM provides a beautiful, simple _ActiveRecord_ implementation for working with databases.

* Each table has a corresponding _model_ which is used to interact with that table.

* If you have completed [**Practical 1**](../practicals/UECS3294-Practical-01.pdf), you would have tried generating a model class using Artisan CLI.

* We can also generate a database migration when generating the model class using the `--migration` or `-m` option:

```
php artisan make:model User --migration

php artisan make:model User -m
```

### Eloquent Model Conventions

* If we have a model named `App\Photo`:

```php
namespace App;

use Illuminate\Database\Eloquent\Model;

class Photo extends Model
{
    //
}
```

#### Table Names

* The abovementioned model named `App\Photo` corresponds to the table named `photos`.

* By convention, the "snake case", plural name of the class will be used as the table name unless another name is explicitly specified.

  * So, in this case, Eloquent will assume the `App\Photo` model stores records in the `photos` table.

* We may specify a custom table by defining a table property on the model:

```php
namespace App;

use Illuminate\Database\Eloquent\Model;

class Photo extends Model
{
	/**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 't_photo';

    //
}
```

#### Primary Key

* Eloquent assumes that each table has a _primary key_ column named `id`.

* To override, define a protected `$primaryKey` property in the model.

* Eloquent also assumes that the primary key is an incrementing integer value, which means that by default the primary key will be cast to an int automatically.

  * To use a non-incrementing or a non-numeric primary key (**not advisable, unless you are developing system based on an old database that you cannot change**) you must set the public `$incrementing` property to `false`.

  * If the primary key is not an integer (**again, not advisable, unless you are developing system based on an old database that you cannot change**), set the protected `$keyType` property to `string`.

#### Timestamps

* By default, Eloquent expects the timestamp columns `created_at` and `updated_at` to exist in the tables.

  * To have these columns **NOT** automatically managed by Eloquent, set the  `$timestamps` property to `false`.

  * To customize the name of the timestamp columns, set the `CREATED_AT` and `UPDATED_AT` constants in the model:

```php
namespace App;

use Illuminate\Database\Eloquent\Model;

class Photo extends Model
{
	const CREATED_AT = 'creation_date';
    const UPDATED_AT = 'last_update';

    //
}
```

#### Database Connections

* By default, all Eloquent models use the default database connection configured for your application.

  * To specify a different connection for the model, set the `$connection` property:.

```php
namespace App;

use Illuminate\Database\Eloquent\Model;

class Photo extends Model
{
	/**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'mysql2nd';

    //
}
```

  * The connection must have been defined in the `config/database.php` as in the example below:

```php
'mysql2nd' => [
    'driver' => 'mysql',
    'host' => 'another.database.com',
    'port' => '3306',
    'database' => 'database_name',
    'username' => 'username',
    'password' => 'password',
    'charset' => 'utf8',
    'collation' => 'utf8_general_ci',
    'prefix' => '',
    'strict' => true,
    'engine' => null,
],
```

### Retrieving Models

* With models defined, we can fluently query the database table associated with the respective models.

```php
use App\Photo;

$photos = App\Photo::all();

foreach ($photos as $photo) {
    echo $photo->title;
}
```

* In the above example, using the `all` method, we perform a query that will return all of the results in the model's table.

#### Collections

* Eloquent methods like `all` and `get` retrieve multiple results. In this case, an instance of `Illuminate\Database\Eloquent\Collection` will be returned.

* The `Collection` class provides a variety of helpful methods for working with your Eloquent results.

  * You can access a column as a property of the object.

  * You can loop through the collection like an array.

#### Adding Additional Constraints using Query Builder

* We may add constraints to queries using the _query builder_, and then use the `get` method to retrieve the results.

  * The `get` method returns an `Illuminate\Support\Collection` instance containing the results where each result is an instance of the PHP `StdClass` object.

```php
$photos = App\Photo::where('status', 1)
	->orderBy('title', 'asc')
	->take(10)
	->get();
```

#### Retrieving a Single Row

* Use the `find` method to query a row by `id` (primary key):

```php
$photo = App\Photo::find(2977);
```

* Use the `first` method to retrieve the first result:

```php
$photo = App\Photo::where('status', 1)->first();
```

* In both the abovementioned methods, the result is an instance of the PHP `StdClass` object.

#### Not Found Exceptions

* Sometimes we may wish to throw an exception if a model is not found.

* This is particularly useful in routes or controllers.

* The `findOrFail` and `firstOrFail` methods will retrieve the first result of the query; however, if no result is found, `Illuminate\Database\Eloquent\ModelNotFoundException` will be thrown:

```php
$photo = App\Photo::findOrFail(2351);

$photo = App\Photo::where('status', 1)->firstOrFail();
```

* If the exception is not caught, a 404 HTTP response is automatically sent back to the user.

* We can also manually throw `Illuminate\Database\Eloquent\ModelNotFoundException`:

```php
$photo = App\Photo::find(8681);
if(!$photo) throw new Illuminate\Database\Eloquent\ModelNotFoundException;
```

#### Retrieving Multiple Rows

* We may use the `find` method with an array of primary keys, which will return a collection of the matching records:

```php
$photos = App\Photo::find([11, 12, 15]);
```

* Of course, as shown earlier, we can retrieve multiple records using the _query builder_.

#### Selecting Columns

* Extract a single column value from a record using the `value` method. This method will return the value of the column directly:

```php
$title = App\Photo::find(2977)->value('title');
```

* To retrieve a `Collection` containing the values of a single column, use the `pluck` method. In this example, we'll retrieve a Collection of photo titles:

```php
$titles = App\Photo::where('status', 1)->pluck('title');

foreach ($titles as $title) {
    echo $title;
}
```

* We may also specify a custom key column for the returned `Collection`:

```php
$titles = App\Photo::where('status', 1)->pluck('title', 'id');

foreach ($titles as $key => $title) {
    //
}
```

#### Chunking Results
* In the event we need to process thousands of Eloquent records, use the `chunk` method.

* The `chunk` method retrieves a _chunk_ of Eloquent models, feeding them to a given `Closure` for processing.

* Using the `chunk` method will conserve memory when working with large result sets:.

```php
Photo::chunk(100, function ($photos) {
    foreach ($photos as $photo) {
        //
    }
});
```

* The first argument passed to the method is the number of records we wish to receive per _chunk_.

* The `Closure` passed as the second argument will be called for each chunk that is retrieved from the database. A database query will be executed to retrieve each chunk of records passed to the `Closure`.

#### Using Cursor

* The `cursor` method allows us to iterate through the records using a _cursor_, which will only execute a single query.

* When processing large amounts of data, the `cursor` method may be used to greatly reduce memory usage:.

```php
foreach (Photo::where('status', 1)->cursor() as $photo) {
    //
}
```

#### Retrieving Aggregates

* We can use the `count`, `sum`, `avg`, `max`, `min` and other _aggregate methods_ provided by the query builder.

* These methods return the appropriate scalar value instead of a full model instance.

* Return the count:

```php
$count = App\Photo::where('status', 1)->count();
```

* Return the maximum:

```php
$max = App\Photo::where('status', 1)->max('votes');
```

### Inserting & Updating Models

#### Insert

* To create a new record in the database, create a new model instance, set attributes on the model, then call the `save` method.

* In the appropriate controller class:

```php
public function store(Request $request)
{
    // Validate the request...

    $photo = new Photo;

    $photo->title = $request->title;
	// Other attributes...

	// OR, use the fill method of model instance if attributes are mass-assignable:
	// $photo->fill($request->all());

    $photo->save();
}
```

* When we call the `save` method, a record will be inserted into the database.

* The `created_at` and `updated_at` timestamps will automatically be set when the `save` method is called, so there is no need to set them manually.

#### Update

* The `save` method may also be used to update models that already exist in the database.

* To update a model, we need to retrieve it, set any attributes we wish to update, and then call the `save` method.

```php
public function update(Request $request)
{
    // Validate the request...

    $photo = new Photo;

    $photo->title = $request->title;
	// Other attributes...

	// OR, use the fill method of model instance if attributes are mass-assignable:
	// $photo->fill($request->all());

    $photo->save();
}
```

* The `updated_at` timestamp will automatically be updated, so there is no need to manually set its value.

#### Mass Updates

* Updates can also be performed against any number of models that match a given query.

* In this example, all photos with the `status` attribute having the value `1` will be updated:

```php
App\Photo::where('status', 1)
	->update(['description' => '']);
```
### Mass Assignment

* We can use the `create` method to save a new model in a single line. The inserted model instance will be returned from the method.

* However, before doing so, we need to specify either a `fillable` or `guarded` attribute on the model, as all Eloquent models protect against mass-assignment by default.

* A mass-assignment vulnerability occurs when a user passes an unexpected HTTP parameter through a request, and that parameter changes a column in our database we did not expect.

  * E.g., a malicious user might send an `is_admin` parameter through an HTTP request, which is then passed into our model's `create` method, allowing the user to escalate themselves to an administrator.

* To get started, we should define which model attributes you want to make mass assignable.

* We do this using the `$fillable` property on the model.

  * E.g., let's make the `title` attribute of the `Photo` model mass assignable:

```php
class Flight extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['title'];
}
```

* Once we have made the attributes mass assignable, we can use the `create` method to insert a new record in the database.

* The `create` method returns the saved model instance::

```php
$photo = App\Photo::create(['title' => 'The Pinnacles, Western Australia']);
```

* If we already have a model instance, we can use the `fill` method to populate it with an array of attributes::

```php
$photo->fill(['title' => 'Cohunu Koala Park, Western Australia']);
```

* Or we can use the `fill` method by passing as parameter an array of input data from the `Request` instance:

```php
$photo->fill($request->all());
```

#### Guarding Attributes

* While `$fillable` serves as a _white list_ of attributes that should be mass assignable, we can choose to use `$guarded`.

* The `$guarded` property should contain an array of attributes that we do not want to be mass assignable.

* All other attributes not in the array will be mass assignable.

* So, `$guarded` functions like a _black list_.

* We should only use either `$fillable` or `$guarded`, but not both.

* In the example below, all attributes except for `votes` will be mass assignable:

```php
class Photo extends Model
{
    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['votes'];
}
```

### Other Creation & Updating Methods

#### The `firstOrCreate` & `firstOrNew` Methods

* The `firstOrCreate` method will attempt to locate a record using the given column/value pairs.

* If the model can not be found in the table, a record will be inserted with the attributes from the first parameter, along with those in the optional second parameter.

```php
// Retrieve photo by title, or create it if it doesn't exist...
$photo = App\Photo::firstOrCreate(['title' => 'Gamcheon Cultural Village']);

// Retrieve photo by name, or create it with the title and description attributes...
$photo = App\Photo::firstOrCreate(
    ['title' => 'Gangnam'], ['description' => 'The upscale Gangnam District of Seoul']
);
```

* The `firstOrNew` method, like `firstOrCreate` will attempt to locate a record in the table matching the given attributes.

* However, if a model is not found, a new model instance will be returned.

* Note that the model returned by `firstOrNew` has not yet been inserted to the table. We will need to call `save` manually to insert it:

```php
// Retrieve photo by title, or instantiate, then call save to insert the record
$photo = App\Photo::firstOrNew(['title' => 'Gamcheon Cultural Village']);
$photo->save();

// Retrieve photo by name, or instantiate with the title and description attributes,
// then call save to insert the record
$photo = App\Photo::firstOrNew(
    ['title' => 'Gangnam'], ['description' => 'The upscale Gangnam District of Seoul']
);
$photo->save();
```

#### The `updateOrCreate` Method

* We may also come across situations where we want to update an existing model or create a new model if none exists.

* Use the `updateOrCreate` method to do this in one step.

* Like the `firstOrCreate` method, `updateOrCreate` persists the model, so there's no need to call `save()`:

```php
// If there's a photo taken in Australia, set the description to "Good-dai!".
// If no matching model exists, create one.
$photo = App\Photo::updateOrCreate(
    ['country' => 'AU'],
    ['description' => 'Good-dai!']
);
```

### Deletion

* To delete a model, call the `delete` method on a model instance:

```php
$photo = App\Photo::find(505);

$photo->delete();
```
#### Deleting An Existing Model By Key

* In the example above, we retrieve the model from the table before calling the `delete` method.

* However, if we know the primary key of the model, we can delete the model without retrieving it. Call the `destroy` method:

```php
App\Photo::destroy(505);

App\Photo::destroy([505, 506, 508]);

App\Photo::destroy(505, 506, 508);
```

#### Deleting Models By Query

* We can run a delete statement on a set of models.

* In this example, we will delete all photos with the `status` attribute having the value `0`:

```php
$deletedPhotos = App\Photo::where('status', 0)->delete();
```

### Soft Deletion

* In addition to actually removing records from the table, Eloquent can perform _soft deletion_ of models.

* When models are soft deleted, they are not actually removed from the table.

* Instead, a `deleted_at` attribute is set on the model and updated into the table.

* If a model has a non-null `deleted_at` value, the model has been soft deleted.

* To enable soft deletes for a model, use the `Illuminate\Database\Eloquent\SoftDeletes` _trait_ on the model and add the `deleted_at` column to the $dates property:

```php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Photo extends Model
{
    use SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];
}
```

* Of course, we need to alter the corresponding table to add the `deleted_at` column. The Laravel schema builder contains a helper method to create this column:

```php
Schema::table('photos', function ($table) {
    $table->softDeletes();
});
```

* When we call the `delete` method on the model, the `deleted_at` column will be set to the current date and time.

* When querying a model that uses soft deletes, the soft deleted models will automatically be excluded from all query results.

* To determine if a given model instance has been soft deleted, use the `trashed` method:

```php
if ($photo->trashed()) {
    //
}
```

#### Including Soft Deleted Models in Queries

* We can force soft deleted models to appear in a result set using the `withTrashed` method on the query:

```php
$photos = App\Photo::withTrashed()
	->where('status', 1)
	->get();
```

* The `withTrashed` method may also be used on a relationship query:

```php
$photo->comments()->withTrashed()->get();
```

#### Retrieving Only Soft Deleted Models in Queries

* The `onlyTrashed` method will retrieve only soft deleted models:

```php
$photos = App\Photo::onlyTrashed()
	->where('status', 1)
	->get();
```

#### Restoring Soft Deleted Models

* Sometimes we may wish to undelete a soft deleted model.

* To restore a soft deleted model into an active state, use the `restore` method on a model instance:

```php
$photo->restore();
```

* We can use the `restore` method in a query to quickly restore multiple models:

```php
App\Photo::withTrashed()
	->where('status', 0)
	->restore();
```

* Like the `withTrashed` method, the `restore` method may also be used on relationships:

```php
$photo->comments()->restore();
```

#### Permanently Deleting Models

* To permanently remove a model from the table, use the `forceDelete` method:

```php
// Force deleting a single model instance...
$photo->forceDelete();

// Force deleting all related models...
$photo->comments()->forceDelete();
```

---

## Relationships

### One-to-one

* E.g.: a `User` model might be associated with one `Phone`.

  * Place a `phone` method on the `User` model. The `phone` method should call the `hasOne` method and return its result:

```php
namespace App;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    /**
     * Get the phone record associated with the user.
     */
    public function phone()
    {
        return $this->hasOne('App\Phone');
    }
}
```

* We also need to define the inverse relationship using the `belongsTo` method:

```php
namespace App;

use Illuminate\Database\Eloquent\Model;

class Phone extends Model
{
    /**
     * Get the user that owns the phone.
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
```

* Eloquent will try to match the `user_id` from the `Phone` model to an `id` on the `User` model.

* Eloquent determines the default foreign key name by examining the name of the relationship method and suffixing the method name with `_id`.

* However, if the foreign key on the `Phone` model is not `user_id`, we can pass a custom key name as the second argument to the `belongsTo` method:

```php
/**
 * Get the user that owns the phone.
 */
public function user()
{
    return $this->belongsTo('App\User', 'userId');
}
```

* If the parent model does not use `id` as its primary key, or if we wish to join the child model to a different column, we can pass a third argument to the `belongsTo` method specifying the parent table's custom key:

```php
/**
 * Get the user that owns the phone.
 */
public function user()
{
    return $this->belongsTo('App\User', 'userId', 'userId');
}
```

#### Default Models

* The `belongsTo` relationship allows us to define a default model that will be returned if the given relationship is `null`.

* This pattern is often referred to as the _Null Object pattern_ and can help remove conditional checks in our code.

* In the following example, the `user` relation will return an empty `App\User` model if no user is attached to the `Post`:

```php
namespace App;

use Illuminate\Database\Eloquent\Model;

class Phone extends Model
{
	/**
	 * Get the author of the post.
	 */
	public function user()
	{
		return $this->belongsTo('App\User')->withDefault();
	}
}
```

* To populate the default model with attributes, you may pass an array or `Closure` to the `withDefault` method:

```php
/**
 * Get the author of the post.
 */
public function user()
{
    return $this->belongsTo('App\User')->withDefault([
        'name' => 'Guest Author',
    ]);
}

/**
 * Get the author of the post.
 */
public function user()
{
    return $this->belongsTo('App\User')->withDefault(function ($user) {
        $user->name = 'Guest Author';
    });
}
```

### One-to-many

* E.g., an album may have many photos:

```php
namespace App;

use Illuminate\Database\Eloquent\Model;

class Album extends Model
{
    /**
     * Get the photos for the album.
     */
    public function photos()
    {
        return $this->hasMany('App\Photo');
    }
}
```

* We also need to define the inverse relationship using the `belongsTo` method as the inverse for a one-to-many relationship is always a one-to-one relationship:

```php
namespace App;

use Illuminate\Database\Eloquent\Model;

class Photo extends Model
{
    /**
     * Get the album that contains the photo.
     */
    public function album()
    {
        return $this->belongsTo('App\Album');
    }
}
```

### Many-to-many

* E.g. a user has many roles, where the roles are also shared by other users.

  * Many users may have the role of "Admin".
  
  * To define this relationship, three database tables are needed: `users`, `roles`, and `role_user`.
  
  * The `role_user` table is the _pivot table_ and its name is derived from the alphabetical order of the related model names, and contains the `user_id` and `role_id` columns.

* Many-to-many relationships are defined by writing a method that returns the result of the `belongsToMany` method:

```php
namespace App;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    /**
     * The roles that belong to the user.
     */
    public function roles()
    {
        return $this->belongsToMany('App\Role');
    }
}
```

* For the inverse relation, we will also define a method that returns the result of the `belongsToMany` method: 

```php
namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    /**
     * The users that belong to the role.
     */
    public function users()
    {
        return $this->belongsToMany('App\User');
    }
}
```
---

## Querying Relations

* Please refer to [**Practical 3**](UECS3294-Practical-03.pdf)

---

## Inserting & Updating Relations

* Please refer to [**Practical 3**](UECS3294-Practical-03.pdf)

---

## Pagination

* Please refer to [**Practical 3**](UECS3294-Practical-03.pdf)

---

## Query Builder

* Laravel's database query builder provides a convenient, fluent interface to creating and running database queries. It can be used to perform most database operations in your application and works on all supported database systems.

* The Laravel query builder uses PDO parameter binding to protect your application against SQL injection attacks. There is no need to clean strings being passed as bindings.

### WHERE Clauses 

#### Without Eloquent ORM

```php
$albums = DB::table('albums')
	->where('status', '1')
	->get();

return view('album.index', ['albums' => $albums]);
```

#### With Eloquent ORM

```php
$albums = App\Album::where('status', '1')->get();

return view('album.index', ['albums' => $albums]);
```

#### Simple Where Clauses

* You may use the `where` method on a query builder instance to add `where` clauses to the query.

* The most basic call to `where` requires three arguments:

  * The first argument is the name of the column.
  
  * The second argument is an operator, which can be any of the database's supported operators.
  
  * Finally, the third argument is the value to evaluate against the column.

* E.g., this is a query that verifies the value of the `votes` column is equal to 100:

```php
$users = App\User::where('votes', '=', 100)->get();
```

* For convenience, if you want to verify that a column is equal to a given value, you may pass the value directly as the second argument to the `where` method:

```php
$users = App\User::where('votes', 100)->get();
```

* Of course, you may use a variety of other operators when writing a where clause:

```php
$users = App\User::where('votes', '>=', 100)->get();

$users = App\User::where('votes', '<>', 100)->get();

$users = App\User::where('name', 'like', '%ee%')->get();
```

* You may also pass an array of conditions to the `where` method:

```php
$users = App\User::where([
    ['status', '=', '1'],
    ['subscribed', '<>', '1'],
])->get();
```

#### Or Statements

* You may chain `where` constraints together as well as add **or** clauses to the query. The  `orWhere` method accepts the same arguments as the `where` method:

```php
$users = App\User::where('votes', '>', 100)
	->orWhere('name', 'Ng Pei Li')
	->get();
```
				
#### whereBetween

* The `whereBetween` method verifies that a column's value is between two values:

```php
$users = App\User::whereBetween('votes', [1, 100])->get();
```

#### whereNotBetween

* The `whereNotBetween` method verifies that a column's value lies outside of two values:

```php
$users = App\User::whereNotBetween('votes', [1, 100])->get();
```

#### whereIn / whereNotIn

* The `whereIn` method verifies that a given column's value is contained within the given array:

```php
$users = App\User::whereIn('id', [1, 2, 3])
	->get();
```
				
* The `whereNotIn` method verifies that the given column's value is not contained in the given array:

```php
$users = App\User::whereNotIn('id', [1, 2, 3])
	->get();
```

#### whereNull / whereNotNull

* The `whereNull` method verifies that the value of the given column is `NULL`:

```php
$users = App\User::whereNull('updated_at')
	->get();
```

* The `whereNotNull` method verifies that the column's value is not `NULL`:

```php
$users = App\User::whereNotNull('updated_at')
	->get();
```

#### whereDate / whereMonth / whereDay / whereYear / whereTime

* The `whereDate` method may be used to compare a column's value against a date:

```php
$users = App\User::whereDate('created_at', '2018-02-16')
	->get();
```

* The `whereMonth` method may be used to compare a column's value against a specific month of a year:

```php
$users = App\User::whereMonth('created_at', '02')
	->get();
```

* The `whereDay` method may be used to compare a column's value against a specific day of a month:

```php
$users = App\User::whereDay('created_at', '16')
	->get();
```

* The `whereYear` method may be used to compare a column's value against a specific year:

```php
$users = App\User::whereYear('created_at', '2018')
	->get();
```

* The `whereTime` method may be used to compare a column's value against a specific time:

```php
$users = App\User::whereTime('created_at', '=', '11:22')
	->get();
```

#### whereColumn

* The `whereColumn` method may be used to verify that two columns are equal:

```php
$users = App\User::whereColumn('first_name', 'last_name')
	->get();
```

* You may also pass a comparison operator to the method:

```php
$users = App\User::whereColumn('updated_at', '>', 'created_at')
	->get();
```

* The `whereColumn` method can also be passed an array of multiple conditions. These conditions will be joined using the **and** operator:

```php
$users = App\User::whereColumn([
	['first_name', '=', 'last_name'],
	['updated_at', '>', 'created_at']
])->get();
```

#### Parameter Grouping

* Sometimes you may need to create more advanced where clauses such as **where exists** clauses or nested parameter groupings.

* The Laravel query builder can handle these as well.

* To get started, let's look at an example of grouping constraints within parenthesis:

```php
$users = App\User::where('name', '=', 'Chia Kim Hooi')
	->orWhere(function ($query) {
		$query->where('votes', '>', 100)
			->where('title', '<>', 'Admin');
	})
    ->get();
```

* Passing a `Closure` into the `orWhere` method instructs the query builder to begin a constraint group.

* The `Closure` will receive a query builder instance which you can use to set the constraints that should be contained within the parenthesis group.

* The example above will produce the following SQL:

```sql
SELECT * FROM users
WHERE name = 'Chia Kim Hooi'
OR (votes > 100 AND title <> 'Admin')
```

#### Where Exists Clauses

* The `whereExists` method allows you to write **where exists** SQL clauses.

* The `whereExists` method accepts a `Closure` argument, which will receive a query builder instance allowing you to define the query that should be placed inside of the **exists** clause:

```php
$users = App\User::whereExists(function ($query) {
	$query->select(DB::raw(1))
		->from('orders')
		->whereRaw('orders.user_id = users.id');
	})
	->get();
```

The query above will produce the following SQL:

```sql
SELECT * FROM users
WHERE EXISTS (
    SELECT 1 FROM orders WHERE orders.user_id = users.id
)
```

#### JSON Where Clauses

* Laravel also supports querying JSON column types on databases that provide support for JSON column types.

* Currently, this includes MySQL 5.7 and PostgreSQL.

* To query a JSON column, use the `->` operator:

```php
$users = App\User::where('options->language', 'en')
	->get();
```

```php
$users = App\User::where('preferences->dining->meal', 'salad')
	->get();
```

### Ordering, Grouping, Limit, & Offset

#### orderBy

* The `orderBy` method allows you to sort the result of the query by a given column.

* The first argument to the `orderBy` method should be the column you wish to sort by, while the second argument controls the direction of the sort and may be either `asc` or `desc`:

```php
$users = App\User::orderBy('name', 'desc')->get();
```

#### latest / oldest

* The `latest` and `oldest` methods allow you to easily order results by date.

* By default, result will be ordered by the `created_at` column. Or, you may pass the column name that you wish to sort by:

```php
$user = App\User::latest()->first();
```

#### inRandomOrder

* The `inRandomOrder` method may be used to sort the query results randomly.

* For example, you may use this method to fetch a random user:

```php
$user = App\User::inRandomOrder()->first();
```

#### groupBy / having

* The `groupBy` and `having` methods may be used to group the query results.

* The `having` method's signature is similar to that of the `where` method:

```php
$users = App\User::groupBy('account_id')
	->having('account_id', '>', 100)
	->get();
```

* You may pass multiple arguments to the `groupBy` method to group by multiple columns:

```php
$users = App\User::groupBy('first_name', 'status')
	->having('account_id', '>', 100)
	->get();
```

#### skip / take

* To limit the number of results returned from the query, or to skip a given number of results in the query, you may use the `skip` and `take` methods:

```php
$users = App\User::skip(10)
	->take(5)
	->get();
```

* Alternatively, you may use the `limit` and `offset` methods:

```php
$users = App\User::offset(10)
	->limit(5)
	->get();
```

### Conditional Clauses

* Sometimes you may want clauses to apply to a query only when something else is true.

* For instance you may only want to apply a `where` statement if a given input value is present on the incoming request.

* You may accomplish this using the `when` method:

```php
$role = $request->input('role');

$users = App\User::when($role, function ($query) use ($role) {
	return $query->where('role_id', $role);
})
->get();
```

* The `when` method only executes the given `Closure` when the first parameter is true. If the first parameter is false, the `Closure` will not be executed.

* You may pass another `Closure` as the third parameter to the `when` method. This `Closure` will execute if the first parameter evaluates as false.

* To illustrate how this feature may be used, we will use it to configure the default sorting of a query:

```php
$sortBy = null;

$users = App\User::when($sortBy, function ($query) use ($sortBy) {
	return $query->orderBy($sortBy);
}, function ($query) {
	return $query->orderBy('name');
})
->get();
```

---
###### The contents in this page are generally based on Laravel's Documentation at https://laravel.com/docs/5.5
</xmp>
<script type="text/javascript" src="https://cdn.rawgit.com/Naereen/StrapDown.js/master/strapdown.min.js?mathjax=y&src=example5&theme=united"></script>
</body>
</html>
