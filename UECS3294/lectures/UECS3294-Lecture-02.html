<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>Lecture 02: The Basics &amp; Architecture Concepts of Laravel</title>
</head>
<body>
<xmp>

# Lecture 02: The Basics & Architecture Concepts of Laravel

---

## Laravel Architecture Concepts

### Lifecycle Overview

* The entry point for all requests to a Laravel application is the `public/index.php` file.

* The `public/index.php` is simply a starting point for loading the rest of the framework.

* The `public/index.php` file loads the Composer generated autoloader definition, and then retrieves an instance of the Laravel application from `bootstrap/app.php` script. 

* The first action taken by Laravel itself is to create an instance of the application/service container.

#### HTTP/Console Kernels

* Next, the incoming request is sent to either the _HTTP kernel_ or the _console kernel_, depending on the type of request that is entering the application.

* These two kernels serve as the central location that all requests flow through.

* For now, let's just focus on the _HTTP kernel_, which is located in `app/Http/Kernel.php`.

  * The HTTP kernel extends the `Illuminate\Foundation\Http\Kernel` class, which defines an array of  bootstrappers that will be run before the request is executed.

  * These bootstrappers configure error handling, configure logging, detect the application environment, and perform other tasks that need to be done before the request is actually handled.

  * The HTTP kernel also defines a list of HTTP _middleware_ that all requests must pass through before being handled by the application.
  
    * These middleware handle reading and writing the HTTP session, determining if the application is in maintenance mode, verifying the CSRF token, and more.

  * The method signature for the HTTP kernel's handle method is quite simple:
  
    * receive a Request and return a Response. 

#### Service Providers

* One of the most important Kernel bootstrapping actions is loading the service providers for your application.

* All of the service providers for the application are configured in the  `config/app.php` configuration file's providers array.

* First, the `register` method will be called on all providers, then, once all providers have been registered, the `boot` method will be called.

* Service providers are responsible for bootstrapping all of the framework's various components, such as the database, queue, validation, and routing components.

* Since they bootstrap and configure every feature offered by the framework, service providers are the most important aspect of the entire Laravel bootstrap process.

#### Dispatch Request
* Once the application has been bootstrapped and all service providers have been registered, the `Request` will be handed off to the router for dispatching.

* The router will dispatch the request to a _route_ or _controller_, as well as run any route specific middleware.

### Service Container

* The Laravel service container is a powerful tool for managing class dependencies and performing dependency injection.

* _Dependency injection_ is a fancy phrase that essentially means this:

  * class dependencies are **injected** into the class via the constructor or, in some cases, **setter** methods.

  
* E.g.:

```php
namespace App\Http\Controllers;

use App\User;
use App\Repositories\UserRepository;
use App\Http\Controllers\Controller;

class UserController extends Controller
{
    /**
     * The user repository implementation.
     *
     * @var UserRepository
     */
    protected $users;

    /**
     * Create a new controller instance.
     *
     * @param  UserRepository  $users
     * @return void
     */
    public function __construct(UserRepository $users)
    {
        $this->users = $users;
    }

    /**
     * Show the profile for the given user.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $user = $this->users->find($id);

        return view('user.profile', ['user' => $user]);
    }
}
```

* In the above example, the `UserController` needs to retrieve users from a data source.

* So, we will inject a service that is able to retrieve users by **type-hinting** in the controller's constructor.

* In this context, our `UserRepository` most likely uses Eloquent to retrieve user information from the database.

* However, since the repository is injected, we are able to easily swap it out with another implementation.

* We are also able to easily "mock", or create a dummy implementation of the `UserRepository` when testing our application.
  
### Service Provider

* Service providers are the central place of all Laravel application bootstrapping. 

* Your own application, as well as all of Laravel's core services are bootstrapped via service providers.

* In general, bootstraping means registering things such service container bindings, event listeners, middleware, and even routes.

* Service providers are the central place to configure your application.

* If you open the `config/app.php` file included with Laravel, you will see a providers array.
  
  * These are all of the service provider classes that will be loaded for your application.
  
  * Many of these are "deferred" providers, meaning they will not be loaded on every request, but only when the services they provide are actually needed.

  * As an example, when we install the _Laravel Collective Forms & HTML_ package, we added a providers as follows:
  
```php
'providers' => [
    // ...
    Collective\Html\HtmlServiceProvider::class,
    // ...
  ],
```

  * You can also write your own service providers and register them with your Laravel application.

### Facades

* Facades provide a **static** interface to classes that are available in the application's service container.

* Laravel ships with many facades which provide access to almost all of Laravel's features.

* Laravel facades serve as **static proxies** to underlying classes in the service container, providing the benefit of a terse, expressive syntax while maintaining more testability and flexibility than traditional static methods.

* All of Laravel's facades are defined in the `Illuminate\Support\Facades` namespace. 

  * We can easily access a facade:

```php
use Illuminate\Support\Facades\Cache;

Route::get('/cache', function () {
    return Cache::get('key');
});
```

### Contracts

* Laravel's Contracts are a set of **interfaces** that define the core services provided by the framework.

  * E.g., the `Illuminate\Contracts\Queue\Queue` contract defines the methods needed for queueing jobs, while the `Illuminate\Contracts\Mail\Mailer` contract defines the methods needed for sending e-mail.

  
* Each contract has a corresponding implementation provided by the framework.

  * E.g., Laravel provides a queue implementation with a variety of drivers, and a mailer implementation that is powered by SwiftMailer.

  
* All of the Laravel contracts live in their own GitHub repository.

  * This provides a quick reference point for all available contracts, as well as a single, decoupled package that may be utilized by package developers.

---

## Eloquent Models

* The **Eloquent ORM** included with Laravel provides a beautiful, simple _ActiveRecord_ implementation for working with your database.

* Each database table has a corresponding **Model** which is used to interact with that table.

* Models allow you to query for data in your tables, as well as insert new records into the table.

* We will look into details about Eloquent Models in the next topic.

---

## Routes

* All Laravel _routes_ are defined in _route files_, which are located in the `routes` directory.

* The route files are automatically loaded.

  * The *routes/web.php* file defines routes that are for your web interface.
  
  * There are 3 other route files in the `routes` directory. We will discuss this in future lectures.
  
### The _routes/web.php_ Route File

* The routes defined in `routes/web.php` is accessible by specifying the defined route's URL in your browser.

* For example, you access the following route by navigating to http://your-domain.com/user in your browser:

```php
Route::get('/user', 'UserController@index');
```

### Basic Routes

* The most basic routes simply accept a URI and a Closure

* This provides a very simple and expressive method of defining routes:

```php
Route::get('/foobar', function () {
    return 'Hello World';
});
```

### Available Router Methods

* The Laravel router (**_NOT your networking router_**) allows you to register routes that respond to any HTTP verb:

```php
Route::get($uri, $callback);
Route::post($uri, $callback);
Route::put($uri, $callback);
Route::patch($uri, $callback);
Route::delete($uri, $callback);
Route::options($uri, $callback);
```

* In situations you need to register a route that responds to multiple HTTP verbs, you may use the `match` method:

```php
Route::match(['get', 'post'], '/foo', function () {
    // Application logic goes here
});
```

* To register a route that responds to all HTTP verbs, use the `any` method:

```php
Route::any('/bar', function () {
    // Application logic goes here
});
```

#### CSRF Protection

* All HTML forms pointing to `POST` `PUT` or `DELETE` routes that are defined in the `routes/web.php` route file must include a CSRF token field. Otherwise, the request will be rejected.

```php
<form method="POST" action="/profile">
    {{ csrf_field() }}
    ...
</form>
```

* **Cross-Site request forgery (CSRF)**, also known as **one-click attack** or **session riding**, is a type of malicious exploit of a website where unauthorized commands are transmitted from a user that the web application trusts.

* There are many ways in which a malicious website can transmit such commands:

  * specially-crafted image tags
  
  * hidden forms
  
  * JavaScript XMLHttpRequests
  
  * etc

* CSRF attacks work without the user's interaction or even knowledge.

* For more information about CSRF attacks, please refer to:

  * [Laravel CSRF Protection](https://laravel.com/docs/5.5/csrf)
  
  * [Wikipedia](https://en.wikipedia.org/wiki/Cross-site_request_forgery)

* If you are using the _Laravel Collective Forms & HTML_ package, the CSRF token used by Laravel for CSRF protection will be added to your forms as a hidden field automatically if you begin a form using the `Form::open` or `Form::model` methods.

  * If you need to generate the HTML for the hidden CSRF field, you may use the `token` method:
  
```php
echo Form::token();
```
  
### Redirect Routes

* To define a route that redirects to another URI, use the `Route::redirect` method:

```php
Route::redirect('/foo', '/bar', 301);
```

* **NOTE**: The _HTTP response status_ code **301 Moved Permanently** in the above example is used for permanent URL redirection

### View Routes

* To define a route that only need to return a view, use the `Route::view` method.

* The `Route::view` method accepts a URI as its first argument and a view name as its second argument.

```php
Route::view('/foobar', 'foobar');
```

* In addition, you may provide an array of data to pass to the view as an optional third argument:

```php
Route::view('/foobar', 'foobar', ['name' => 'Chia Kim Hooi']);
```

### Route Parameters

#### Required parameters

* In some situations, you will need to capture segments of the URI within your route.

  * E.g., you may need to capture a user's ID from the URL.
  
  * To do so, define route parameters:
  
```php
Route::get('user/{id}', function ($id) {
    return 'User '.$id;
});
```

  * The above route can be accessed by navigating to http://your-domain.com/user/7453 where **7453** is the value passed to the parameter `id`:

* You can also define multiple parameters within a route:

```php
Route::get('posts/{post}/comments/{comment}', function ($postId, $commentId) {
    // Application logic goes here
});
```

* Route parameters are always enclosed within `{}` braces and should consist of alphabetic characters, and may not contain a `-` (dash) character.

  * Instead of using the `-` character, use an underscore (`_`).
  
  * Route parameters are injected into route callbacks/controllers based on their order
  
    * The names of the callback/controller arguments do **not** matter.

#### Optional parameters

* If you need to specify a route parameter, but make the presence of that route parameter is optional, place a `?` mark after the parameter name.

* Ensure that you give the route's corresponding variable a default value:

```php
Route::get('user/{name?}', function ($name = null) {
    return $name;
});
```

```php
Route::get('user/{name?}', function ($name = 'Ng Pei Li') {
    return $name;
});
```

### Regular Expression Constraints

* You can constrain the format of your route parameters using the `where` method on a route instance.

* The `where` method accepts the name of the parameter and a **regular expression** defining how the parameter should be constrained:

```php
Route::get('user/{name}', function ($name) {
    //
})->where('name', '[A-Za-z]+');
```

```php
Route::get('user/{id}', function ($id) {
    //
})->where('id', '[0-9]+');
```

```php
Route::get('user/{id}/{name}', function ($id, $name) {
    //
})->where(['id' => '[0-9]+', 'name' => '[a-z]+']);
```

#### Global Constraints

* To specify that a route parameter is always be constrained by a given regular expression, you may use the `pattern` method.

* Define these patterns in the `boot` method of your `RouteServiceProvider`:

```php
/**
 * Define your route model bindings, pattern filters, etc.
 *
 * @return void
 */
public function boot()
{
    Route::pattern('id', '[0-9]+');

    parent::boot();
}
```

* Once the pattern has been defined, it is automatically applied to all routes using that parameter name:

```php
Route::get('user/{id}', function ($id) {
    // Only executed if {id} is numeric...
});
```

### Named Routes

* Named routes allow the convenient generation of URLs or redirects for specific routes.

* You may specify a name for a route by chaining the `name` method onto the route definition:

```php
Route::get('user/profile', function () {
    // Application logic goes here
})->name('user.profile');
```

* You may also specify route names for controller actions:

```php
Route::get('user/profile', 'UserController@showProfile')->name('user.profile');
```

#### Generating URLs to Named Routes

* Once you have assigned a name to a given route, you may use the route's name when generating URLs or redirects via the global `route` function:

```php
// Generating URLs...
$url = route('user.profile');

// Generating Redirects...
return redirect()->route('user.profile');
```

* If the named route contains parameters, pass the parameters as the second argument to the `route` function:

```php
Route::get('user/{id}/profile', function ($id) {
    //
})->name('user.profile');
```

```php
$url = route('user.profile', ['id' => 8681]);
```

---

## Controllers

* You have seen that we can define our request handling logic as Closures within a route in a route file.

* However, instead of doing so, it will be a better idea to organize the request handling logic using **controller** classes.

* Controllers group related request handling logic into a single class.

* The `app/Http/Controllers` directory contains the controller classes.

### Defining Controllers

* The code below is an example of a controller class:

```php
namespace App\Http\Controllers;

use App\User;
use App\Http\Controllers\Controller;

class UserController extends Controller
{
    /**
     * Show the profile for the given user.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        return view('user.profile', ['user' => User::findOrFail($id)]);
    }
}
```

* A controller class extends the base controller class included with Laravel.

  * The base class provides a few convenience methods such as the `middleware` method, which may be used to attach _middleware_ to controller actions.
  
  
* Define a route to the `show` method of `UserController`:

```php
Route::get('/user/show/{id}', 'UserController@show');
```

* Using _named routes_:

```php
Route::get('/user/show/{id}', 'UserController@show')->name('user.show');
```

* When a request matches the specified route URI, the `show` method on the `UserController` class will be executed.

  * Any route parameters will also be passed to the method.

### Namespaces of Controllers

* It is very important to note that we did not need to specify the full controller namespace when defining the controller route.

* Since the `RouteServiceProvider` loads your route files within a route group that contains the namespace, we only specified the portion of the class name that comes after the `App\Http\Controllers` portion of the namespace.

* If you choose to nest your controllers deeper into the `App\Http\Controllers` directory, simply use the specific class name relative to the `App\Http\Controllers` root namespace.

* So, if your full controller class is `App\Http\Controllers\Photos\AdminController`, you should register routes to the controller as follows:

```php
Route::post('/photos/upload/{id}', 'Photos\AdminController@upload');
```

### Single Action Controllers

* To define a controller that only handles a single action, define a single `__invoke` method on the controller:

```php
namespace App\Http\Controllers;

use App\User;
use App\Http\Controllers\Controller;

class ShowProfile extends Controller
{
    /**
     * Show the profile for the given user.
     *
     * @param  int  $id
     * @return Response
     */
    public function __invoke($id)
    {
        return view('user.profile', ['user' => User::findOrFail($id)]);
    }
}
```

* To register routes for single action controllers, you do not need to specify a method:

```php
Route::get('user/{id}', 'ShowProfile');
```

### Dependency Injection & Controllers

#### Constructor Injection

* The Laravel service container is used to resolve all Laravel controllers.

* Hence, you can **type-hint** any dependencies that your controller may need in its constructor.

* The declared dependencies will automatically be resolved and injected into the controller instance:

```php
namespace App\Http\Controllers;

use App\Repositories\UserRepository;

class UserController extends Controller
{
    /**
     * The user repository instance.
     */
    protected $users;

    /**
     * Create a new controller instance.
     *
     * @param  UserRepository  $users
     * @return void
     */
    public function __construct(UserRepository $users)
    {
        $this->users = $users;
    }
}
```

#### Method Injection

* To **type-hint** dependencies on your controller's methods:

```php
namespace App\Http\Controllers;

use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * Store a new user.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        $name = $request->name;

        //
    }
}
```

* A common use-case for method injection is injecting the `Illuminate\Http\Request` instance into your controller methods. This provides access to the `Illuminate\Http\Request` instance within the method, for e.g. to retrieve form input.

### Route Caching

* If your application is exclusively using controller based routes, you should take advantage of Laravel's route cache.

* Using the route cache will drastically decrease the amount of time it takes to register all of your application's routes.

* In some cases, your route registration may even be up to 100 times faster.

* To generate a route cache, just execute the `route:cache` Artisan command:

```
php artisan route:cache
```

* After running this command, your cached routes file will be loaded on every request.

* However, if you add any new routes you will need to generate a fresh route cache.

  * Because of this, you should only run the `route:cache` command during your project's deployment.

  
* You may use the `route:clear` command to clear the route cache:

```
php artisan route:clear
```

---

## Views

* Views contain the HTML served by your application and separate your controller/application logic from your presentation logic.

* Views are stored in the `resources/views` directory.

* A very simple view using the **Blade template** that is stored in `resources/views/greeting.blade.php` might look something like this:

```php
<!DOCTYPE html>
<html>
    <body>
        <h1>Hello, {{ $name }}</h1>
    </body>
</html>
```

* We may return it using the global `view` helper:

```php
Route::get('/', function () {
    return view('greeting', ['name' => 'Chia Kim Hooi']);
});
```

* The first argument passed to the `view` helper corresponds to the name of the view file in the `resources/views` directory.

* The second argument is an array of data that should be made available to the view. In this case, we are passing the `name` variable, which is displayed in the view using Blade syntax.

* Views may also be nested within sub-directories of the `resources/views` directory. **Dot notation** is used to reference nested views.

  * E.g., if your view is stored at `resources/views/admin/profile.blade.php`, reference it this way:

```php
return view('admin.profile', ['name' => 'Foo Yoke Wai']);
```

---

## Middleware

* **Middleware** provides a convenient mechanism for filtering HTTP requests entering your application.

  * E.g., Laravel includes a middleware that verifies the user of your application is authenticated.
  
    * If the user is not authenticated, the middleware will redirect the user to the login screen.
	
	* Otherwise, if the user is authenticated, the middleware will allow the request to proceed further into the application.

* Additional middleware can be written to perform a variety of tasks.

* Laravel includes several middleware in the framework, including middleware for authentication and CSRF protection.

* The middleware are located in the `app/Http/Middleware` directory.

* The code below shows an example middleware:

```php
namespace App\Http\Middleware;

use Closure;

class CheckAge
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->age <= 12) {
            return redirect('home');
        }

        return $next($request);
    }
}
```

* In this middleware, we will only allow access to the route if the supplied age is greater than 12. Otherwise, we will redirect the users back to the home URI.

* Calling the `$next` callback with the `$request` as parameter allows us to pass the request deeper into the application.

### Before & After Middleware

* Whether a middleware runs before or after a request depends on the middleware itself.

#### Before Middleware

* The following middleware would perform some task before the request is handled by the application:

```php
namespace App\Http\Middleware;

use Closure;

class BeforeMiddleware
{
    public function handle($request, Closure $next)
    {
        // Perform some action first
		// ...

		// Then only call the $next callback
        return $next($request);
    }
}
```

#### After Middleware

* The following middleware would perform some task after the request is handled by the application:

```php
namespace App\Http\Middleware;

use Closure;

class AfterMiddleware
{
    public function handle($request, Closure $next)
    {
		// Call the $next callback first
        $response = $next($request);

        // Then only perform some action

		// Finally, return the response
        return $response;
    }
}
```

### Registering Middleware

* After defining the middleware class, how would you use it?

#### Global Middleware

* Runs during every HTTP request to your application.

* To do so, list the middleware class in the `$middleware` property of `app/Http/Kernel` class:

```php
protected $middleware = [
    // ...
	// ...
    \App\Http\Middleware\CheckAge::class,
];
```

#### Route Middleware

* First, assign the middleware a _key_ in `app/Http/Kernel`.

* By default, the `$routeMiddleware` property of this class contains entries for the middleware included with Laravel.

* To add your middleware, append it to this list and assign it a key of your choosing:

```php
protected $routeMiddleware = [
    'auth' => \Illuminate\Auth\Middleware\Authenticate::class,
    'auth.basic' => \Illuminate\Auth\Middleware\AuthenticateWithBasicAuth::class,
    'bindings' => \Illuminate\Routing\Middleware\SubstituteBindings::class,
    'can' => \Illuminate\Auth\Middleware\Authorize::class,
    'guest' => \App\Http\Middleware\RedirectIfAuthenticated::class,
    'throttle' => \Illuminate\Routing\Middleware\ThrottleRequests::class,
];
```

* You can then use the `middleware` method to assign middleware to a route:

```php
Route::get('/admin/profile', function () {
    //
})->middleware('auth')
```

* Assigning multipe middleware:

```php
Route::get('/admin/manage', function () {
    //
})->middleware('auth', 'can');
```

* Alternatively, you may also provide a fully-qualified class name:

```php
use App\Http\Middleware\CheckAge;

Route::get('admin/profile', function () {
    //
})->middleware(CheckAge::class);
```

#### Controller Middleware

* To assign middleware to controllers, you may specify it in the route file:

```php
Route::get('profile', 'UserController@show')->middleware('auth');
```

* However, it is more convenient to specify middleware within the controller's constructor using the `middleware` method:

```php
class UserController extends Controller
{
    /**
     * Instantiate a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
		// Assign the middleware 'auth' to this controller
        $this->middleware('auth');

		// Assign the middleware 'log' to this controller
		// only for the index method
        $this->middleware('log')->only('index');

		// Assign the middleware 'subscribed' to this controller
		// except the store method
        $this->middleware('subscribed')->except('store');
    }
}
```

##### Using Closure as Middleware

* Within a controller, it is possible to register middleware using a Closure.

* This provides a convenient way to define a middleware for a single controller without defining an entire middleware class:

```php
$this->middleware(function ($request, $next) {
    // ...

    return $next($request);
});
```

### Middleware Groups

* In some cases, you may want to group several middleware under a single key to make them easier to assign to routes.

* You may do this using the `$middlewareGroups` property in `app/Http/Kernel`.

* By default, Laravel defines the `web` and `api` middleware groups that contains common middleware you may want to apply to your web UI and API routes:

```php
/**
 * The application's route middleware groups.
 *
 * @var array
 */
protected $middlewareGroups = [
    'web' => [
        \App\Http\Middleware\EncryptCookies::class,
        \Illuminate\Cookie\Middleware\AddQueuedCookiesToResponse::class,
        \Illuminate\Session\Middleware\StartSession::class,
        \Illuminate\View\Middleware\ShareErrorsFromSession::class,
        \App\Http\Middleware\VerifyCsrfToken::class,
        \Illuminate\Routing\Middleware\SubstituteBindings::class,
    ],

    'api' => [
        'throttle:60,1',
        'auth:api',
    ],
];
```

---

## HTTP Request & Response

### HTTP Request

* You can obtain an instance of the current HTTP request using dependency injection by **type-hinting** the `Illuminate\Http\Request` class on your controller method:

```php
namespace App\Http\Controllers;

use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * Store a new user.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        $name = $request->input('name');

        //
    }
}
```

* If your controller method is also expecting input from a route parameter, you should list your route parameters after your other dependencies:

```php
namespace App\Http\Controllers;

use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * Update the specified user.
     *
     * @param  Request  $request
     * @param  string  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }
}
```

* You can also type-hint the `Illuminate\Http\Request` class on a route Closure:

```php
use Illuminate\Http\Request;

Route::get('/', function (Request $request) {
    // 
});
```

#### Retrieving Request Path & Method

##### Retrieving Request Path

* The path method returns the request's path information.

* E.g.: If the incoming request is targeted at `http://domain.com/foo/bar`, the path method will return `foo/bar`:

```php
$uri = $request->path();
```

* The `is` method allows you to verify that the incoming request path matches a given pattern.

* You may use the `*` wildcard character when utilizing this method:

```php
if ($request->is('admin/*')) {
    //
}
```

##### Retrieving Request URL

* To retrieve the full URL for the incoming request, use the `url` or `fullUrl` methods.

* The `url` method returns the URL without the query string:

```php
$url = $request->url();
```

* The `fullUrl` method returns the URL including the query string:

```php
$url = $request->fullUrl();
```

##### Retrieving Request Method

* The `method` method returns the HTTP verb for the request:

```php
$method = $request->method();
```

* Use the `isMethod` method to verify that the HTTP verb matches a given string:

```php
if ($request->isMethod('post')) {
    //
}
```

#### Input Trimming & Normalization

* Fortunately, or unfortunately, depending on your requirements, by default, Laravel includes the `TrimStrings` and `ConvertEmptyStringsToNull` middleware in your application's global middleware stack.

* These middleware are listed in the stack by the `App\Http\Kernel` class.

* These middleware will automatically trim all incoming string fields on the request, as well as convert any empty string fields to null.

* This allows you to not have to worry about these normalization concerns in your routes and controllers.

* If you would like to disable this behavior, you may remove the two middleware from your application's middleware stack by removing them from the `$middleware` property of your `App\Http\Kernel` class.

#### Retrieving Input

##### Retrieving All Input Data

```php
$input = $request->all();
```

##### Retrieving An Input Value

```php
$name = $request->input('name');
```

* If the HTML form contains array inputs, use the **dot** notation to access the arrays:

```php
// Retrieve at position 0
$name = $request->input('products.0.name');

// Retrieve all
$names = $request->input('products.*.name');
```

##### Retrieving An Input Value with Default Value

* The default value will be returned if the requested input value is not present on the request:

```php
$name = $request->input('name', 'Foo Yoke Wai');
```


##### Retrieving Input from Query String

```php
// Without default value
$name = $request->query('name');

// With default value
$name = $request->query('name', 'Lim Li Li');
```

* Call the `query` method without any arguments in order to retrieve all of the query string values as an associative array:

```php
$query = $request->query();
```

##### Retrieving Input Via Dynamic Properties

* Access user input using dynamic properties on the `Illuminate\Http\Request` instance. 

* E.g., if a form contains a `name` field, access the value of the field this way:

```php
$name = $request->name;
```

* When using dynamic properties, Laravel will first look for the parameter's value in the request payload. If it is not present, Laravel will search for the field in the route parameters.

##### Retrieving JSON Input Values

* When sending JSON requests to your application, you can access the JSON data via the `input` method as long as the `Content-Type` header of the request is properly set to `application/json`.

* Use **dot** syntax to dig into JSON objects:

```php
$name = $request->input('user.name');
```

##### Retrieving A Portion Of The Input Data

```php
$input = $request->only(['username', 'password']);

$input = $request->only('username', 'password');

$input = $request->except(['credit_card']);

$input = $request->except('credit_card');
```

##### Determining If An Input Value Is Present

* Use the `has` method to determine if a value is present on the request:

```php
if ($request->has('name')) {
    //
}
```

* When given an array, the `has` method will determine if all of the specified values are present:

```php
if ($request->has(['name', 'email'])) {
    //
}
```

* To determine if a value is present on the request and is not empty, use the `filled` method:

```php
if ($request->filled('name')) {
    //
}
```

#### File Upload

##### Retrieving Uploaded Files

* Access uploaded files from a `Illuminate\Http\Request` instance using the `file` method or using _dynamic properties_.

* The `file` method returns an instance of the `Illuminate\Http\UploadedFile` class, which extends the PHP `SplFileInfo` class and provides a variety of methods for interacting with the file:

```php
$file = $request->file('photo');
```

* Using _dynamic properties_:

```php
$file = $request->photo;
```

* Determine if a file is present on the request using the `hasFile` method:

```php
if ($request->hasFile('photo')) {
    //
}
```

##### Validation Successful File Uploads

* Verify that there were no problems uploading the file using the `isValid` method:

```php
if ($request->file('photo')->isValid()) {
    //
}
```

##### File Paths & Extensions

* The `UploadedFile` class also contains methods for accessing the file's fully-qualified path and its extension.

* The `path` method returns the path:

```php
$path = $request->photo->path();
```


* The `extension` method will attempt to guess the file's extension based on its contents. This extension may be different from the extension that was supplied by the client:

```php
$extension = $request->photo->extension();
```

##### Storing Uploaded Files

* Use `store` method of the `Illuminate\Http\UploadedFile` class which will move an uploaded file to a location on your local filesystem or even a cloud storage location.

* The method accepts the **path** where the file should be stored relative to the **filesystem's configured root directory**.

* This path should not contain a file name, since a unique ID will automatically be generated to serve as the file name:

```php
$path = $request->photo->store('images');
```

* If you do not want a file name to be automatically generated, use the `storeAs` method, which accepts the path & file name as arguments:

```php
$path = $request->photo->storeAs('images', 'filename.jpg');
```

### HTTP Response

#### Creating Responses

* All routes and controllers should return a response to be sent back to the user's browser.

##### Strings

* Converts a string into a full HTTP response:

```php
Route::get('/', function () {
    return 'Hello World';
});
```

##### Arrays

* Converts the array into a JSON response:

```php
Route::get('/', function () {
    return [1, 2, 3];
});
```

##### Response Objects

* Typically, you won't just be returning simple strings or arrays from your route or controller actions.

* Instead, you will be returning full `Illuminate\Http\Response` instances as it allows you to customize the response's HTTP status code and headers:

```php
Route::get('home', function () {
    return response('Hello World', 200)
        ->header('Content-Type', 'text/plain');
});
```

* Multiple headers:

```php
return response($content)
    ->header('Content-Type', $type)
	->header('X-Header-One', 'Header Value')
    ->header('X-Header-Two', 'Header Value');
```

* Alternatively, use the `withHeaders` method:

```php
return response($content)
    ->withHeaders([
        'Content-Type' => $type,
        'X-Header-One' => 'Header Value',
        'X-Header-Two' => 'Header Value',
    ]);
```

##### View Responses

* More often than not, you will be returning a **view** as the response content:

```php
return view('albums.create');
```

* Passing data to view:

```php
return view('albums.index', [
    'albums' => $albums
]);
```

##### JSON Responses

* The `json` method will automatically set the `Content-Type` header to `application/json`, as well as convert the given array to JSON using the `json_encode` PHP function:

```php
return response()->json([
    'name' => 'Tan Aik Khoon',
    'country' => 'MY'
]);
```

##### File Downloads

* The `download` method may be used to generate a response that forces the user's browser to download the file at the given path:

```php
return response()->download($pathToFile);
```

* The `download` method may accept a file name as the second argument to the method, which will determine the file name that is seen by the user downloading the file:

```php
return response()->download($pathToFile, $name);
```

* You may pass an array of HTTP headers as the third argument to the method:

```php
return response()->download($pathToFile, $name, $headers);
```

* Chain the `deleteFileAfterSend` method with parameter set to `true` to delete the file after sending:

```php
return response()->download($pathToFile)->deleteFileAfterSend(true);
```

##### File Responses

* The `file` method may be used to display a file, such as an image or PDF, directly in the user's browser instead of initiating a download.

* This method accepts the path to the file as its first argument:

```php
return response()->file($pathToFile);
```

* You may pass an array of headers as its second argument:

```php
return response()->file($pathToFile, $headers);
```

#### Redirects

##### Simple Redirects

* Redirect responses are instances of the `Illuminate\Http\RedirectResponse` class, and contain the proper headers needed to redirect the user to another URL:

```php
Route::get('/dashboard', function () {
    return redirect('home/dashboard');
});
```
* To redirect the user to their previous location, such as when a submitted form is invalid:

```php
Route::post('/album/store', function () {
    // Validate the request...

    return back()->withInput();
});
```

##### Redirecting to Named Routes

```php
return redirect()->route('login');
```

* With parameters:

```php
// For a route with the following URI: /album/{id}

return redirect()->route('album', ['id' => 2977]);
```

* Populating Parameters Via Eloquent Models:

```php
// For a route with the following URI: /album/{id}

return redirect()->route('album', [$album]);
```

##### Redirecting to Controller Actions

```php
return redirect()->action('AlbumController@index');
```

* With parameters:

```php
return redirect()->action(
    'AlbumController@show', ['id' => 2977]
);
```

##### Redirecting To External Domains

```php
return redirect()->away('https://www.google.com');
```

---

###### The contents in this page are generally based on Laravel's Documentation at https://laravel.com/docs/5.5
</xmp> 
<script type="text/javascript" src="https://cdn.rawgit.com/Naereen/StrapDown.js/master/strapdown.min.js?mathjax=y&src=example5&theme=united"></script>
</body>
</html>
