<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>Managing Application Dependencies, Assets &amp; Data Migration</title>
</head>
<body>
<xmp>

# Lecture 07: Managing Application Dependencies, Assets & Data Migration

---

## Managing Application Dependencies

* Recall that we have used **Composer** to kick start a Laravel project by installing Laravel using the command line at the beginning of this course.

* We have also used Composer to install additional packages that we require when developing our Laravel app, such as _Laravel Collective: Forms & HTML_ and _Bouncer_.

* By using Composer to install Laravel and the additional packages, we are using a _dependency manager_ to manage the dependencies in our application.

### Dependency Management

* Composer is a tool for dependency management in PHP. It allows you to declare the libraries your project depends on and it will manage (install/update) them for you. 

* Various other package/dependency management software are available for other programming/scripting languages. E.g. you will use *NPM* to manage application dependencies for JavaScript or *Pip* for Python.

* The idea behind the use of a dependency manager is as follows:

  * Suppose that:
  
    1. You have a project that depends on a number of libraries.

	2. Some of those libraries depend on other libraries.
	
  * Using Composer:
  
    1. Enables you to declare the libraries you depend on.
	
	2. Finds out which versions of which packages can and need to be installed, and installs them (meaning it downloads them into your project).

### Composer

* To start using Composer in your project, all you need is a `composer.json` file. This file describes the dependencies of your project and may contain other metadata as well.

* Laravel ships with a `composer.json` file with all the required dependencies for a standard Laravel installation already declared.

* The `composer.json` file for a fresh Laravel 5.5 installation contains the following:

```javascript
{
    "name": "laravel/laravel",
    "description": "The Laravel Framework.",
    "keywords": ["framework", "laravel"],
    "license": "MIT",
    "type": "project",
    "require": {
        "php": ">=7.0.0",
        "fideloper/proxy": "~3.3",
        "laravel/framework": "5.5.*",
        "laravel/tinker": "~1.0"
    },
    "require-dev": {
        "filp/whoops": "~2.0",
        "fzaninotto/faker": "~1.4",
        "mockery/mockery": "~1.0",
        "phpunit/phpunit": "~6.0",
        "symfony/thanks": "^1.0"
    },
    "autoload": {
        "classmap": [
            "database/seeds",
            "database/factories"
        ],
        "psr-4": {
            "App\\": "app/"
        }
    },
    "autoload-dev": {
        "psr-4": {
            "Tests\\": "tests/"
        }
    },
    "extra": {
        "laravel": {
            "dont-discover": [
            ]
        }
    },
    "scripts": {
        "post-root-package-install": [
            "@php -r \"file_exists('.env') || copy('.env.example', '.env');\""
        ],
        "post-create-project-cmd": [
            "@php artisan key:generate"
        ],
        "post-autoload-dump": [
            "Illuminate\\Foundation\\ComposerScripts::postAutoloadDump",
            "@php artisan package:discover"
        ]
    },
    "config": {
        "preferred-install": "dist",
        "sort-packages": true,
        "optimize-autoloader": true
    }
}
```

* As we have installed _Laravel Collective: Forms & HTML_ and _Bouncer_, our `composer.json` file will contain the declaration for the dependencies for both packages:

```javascript
{
    "name": "laravel/laravel",
    "description": "The Laravel Framework.",
    "keywords": ["framework", "laravel"],
    "license": "MIT",
    "type": "project",
    "require": {
        "php": ">=7.0.0",
        "fideloper/proxy": "~3.3",
        "laravel/framework": "5.5.*",
        "laravel/tinker": "~1.0",
        "laravelcollective/html": "^5.4.0",
        "silber/bouncer": "v1.0.0-rc.1"
    },
    "require-dev": {
        "filp/whoops": "~2.0",
        "fzaninotto/faker": "~1.4",
        "mockery/mockery": "~1.0",
        "phpunit/phpunit": "~6.0"
    },
    "autoload": {
        "classmap": [
            "database/seeds",
            "database/factories"
        ],
        "psr-4": {
            "App\\": "app/"
        }
    },
    "autoload-dev": {
        "psr-4": {
            "Tests\\": "tests/"
        }
    },
    "extra": {
        "laravel": {
            "dont-discover": [
            ]
        }
    },
    "scripts": {
        "post-root-package-install": [
            "@php -r \"file_exists('.env') || copy('.env.example', '.env');\""
        ],
        "post-create-project-cmd": [
            "@php artisan key:generate"
        ],
        "post-autoload-dump": [
            "Illuminate\\Foundation\\ComposerScripts::postAutoloadDump",
            "@php artisan package:discover"
        ]
    },
    "config": {
        "preferred-install": "dist",
        "sort-packages": true,
        "optimize-autoloader": true
    }
}

```

#### The `require` key

* The first (and often only) thing you specify in `composer.json` is the `require` key.
 
* You are simply telling Composer which packages your project depends on:

```javascript
"require": {
    "php": ">=7.0.0",
    "fideloper/proxy": "~3.3",
    "laravel/framework": "5.5.*",
    "laravel/tinker": "~1.0",
    "laravelcollective/html": "^5.4.0",
    "silber/bouncer": "v1.0.0-rc.1"
},
```

* `require` takes an object that maps package names (e.g. `laravel/framework`) to version constraints (e.g. `5.5.*`).

* Composer uses this information to search for the right set of files in package repositories that you register using the repositories key, or in *Packagist*, the default package repository.

#### Package Names

* The package name consists of a vendor name and the project's name.

* Often these will be identical - the vendor name just exists to prevent naming clashes.

* E.g., it would allow two different people to create a library named `json`. One might be named `igorw/json` while the other might be `seldaek/json`.

#### Package Version Constraints

* In the above example, one of the packages we are requesting is the `laravel/framework` package with the version constraint `5.5.*`.

* This means any version in the 5.5 development branch, or any version that is greater than or equal to 5.5 and less than 5.6 (>=5.5 && <5.6).

##### Exact Version Constraint

* You can specify the exact version of a package. This will tell Composer to install this version and this version only. If other dependencies require a different version, the solver will ultimately fail and abort any install or update procedures:

```
2.16.18
```

##### Version Range

* Using comparison operators you can specify ranges of valid versions. Valid operators are `>`, `>=`, `<`, `<=`, `!=`.

* You can define multiple ranges. Ranges separated by a space or comma will be treated as a logical *AND*. A double pipe (`||`) will be treated as a logical *OR*. *AND* has higher precedence than *OR*.

```
>=1.0
>=1.0 <2.0
>=1.0 <1.1 || >=1.2
```

##### Hyphenated Version Range ( `-` )

* Inclusive set of versions. Partial versions on the right include are completed with a wildcard. 

* E.g., `1.0 - 2.0` is equivalent to `>=1.0.0 <2.1` as the `2.0 `becomes `2.0.*`.

* On the other hand `1.0.0 - 2.1.0` is equivalent to `>=1.0.0 <=2.1.0`.


##### Wildcard Version Range (.*)

* You can specify a pattern with a `*` wildcard.

* `1.0.*` is the equivalent of `>=1.0 <1.1`

##### Next Significant Release Operators: Tilde Version Range (`~`)

* The `~` operator is best explained by example:

  * `~1.2` is equivalent to `>=1.2 <2.0.0`
  
  * `~1.2.3` is equivalent to `>=1.2.3 <1.3.0`
  
  * A common usage would be to mark the minimum minor version you depend on, like `~1.2` (which allows anything up to, but not including, 2.0).

##### Next Significant Release Operators: Caret Version Range (`^`)

* The `^` operator behaves very similarly but it sticks closer to semantic versioning, and will always allow non-breaking updates.

  * `^1.2.3` is equivalent to `>=1.2.3 <2.0.0` as none of the releases until 2.0 should break backwards compatibility.
  
  * For pre-1.0 versions it also acts with safety in mind and treats `^0.3` as `>=0.3.0 <0.4.0`

#### Stability Constraints

* If you are using a constraint that does not explicitly define a stability, Composer will default internally to `-dev` or `-stable`, depending on the operator(s) used. This happens transparently.

If you wish to explicitly consider only the stable release in the comparison, add the suffix `-stable`.

Examples:

| Constraint | Internally |
| ---- | -------------------- |
| 1.2.3 | =1.2.3.0-stable |
| >1.2 | >1.2.0.0-stable |
| >=1.2 | >=1.2.0.0-dev |
| >=1.2-stable | >=1.2.0.0-stable |
| <1.3 | <1.3.0.0-dev |
| <=1.3 | <=1.3.0.0-stable |
| 1 - 2 | >=1.0.0.0-dev <3.0.0.0-dev |
| ~1.3 | >=1.3.0.0-dev <2.0.0.0-dev |
| 1.4.* | >=1.4.0.0-dev <1.5.0.0-dev |

#### Summary

```javascript
"require": {
    "vendor/package": "1.3.2", // exactly 1.3.2

    // >, <, >=, <= | specify upper / lower bounds
    "vendor/package": ">=1.3.2", // anything above or equal to 1.3.2
    "vendor/package": "<1.3.2", // anything below 1.3.2

    // * | wildcard
    "vendor/package": "1.3.*", // >=1.3.0 <1.4.0

    // ~ | allows last digit specified to go up
    "vendor/package": "~1.3.2", // >=1.3.2 <1.4.0
    "vendor/package": "~1.3", // >=1.3.0 <2.0.0

    // ^ | doesn't allow breaking changes (major version fixed - following semver)
    "vendor/package": "^1.3.2", // >=1.3.2 <2.0.0
    "vendor/package": "^0.3.2", // >=0.3.2 <0.4.0 // except if major version is 0
}
```

#### Installing Dependencies

* To install the defined dependencies for your project, just run the `install` command.

```
composer install
```

When you run this command, one of two things may happen:

##### Installing Without `composer.lock`

* If you have never run the command before and there is also no `composer.lock` file present, Composer simply resolves all dependencies listed in your `composer.json` file and downloads the latest version of their files into the vendor directory in your project.

* When Composer has finished installing, it writes all of the packages and the exact versions of them that it downloaded to the `composer.lock` file, locking the project to those specific versions. You should commit the `composer.lock` file to your project repo so that all people working on the project are locked to the same versions of dependencies.

##### Installing With composer.lock

* This brings us to the second scenario. If there is already a `composer.lock` file as well as a `composer.json` file when you run `install`, it means either you ran the `install` command before, or someone else on the project ran the `install` command and committed the `composer.lock` file to the project (which is good).

* Either way, running `install` when a `composer.lock` file is present resolves and installs all dependencies that you listed in `composer.json`, but Composer uses the exact versions listed in composer.lock to ensure that the package versions are consistent for everyone working on your project.

* As a result you will have all dependencies requested by your `composer.json` file, but they may not all be at the very latest available versions (some of the dependencies listed in the `composer.lock` file may have released newer versions since the file was created).

* This is by design, it ensures that your project does not break because of unexpected changes in dependencies.

##### Commit Your composer.lock File to Version Control

* Committing this file to VC is important because it will cause anyone who sets up the project to use the exact same versions of the dependencies that you are using.

* Your development server, production machines, other developers in your team, everything and everyone runs on the same dependencies, which mitigates the potential for bugs affecting only some parts of the deployments.

* Even if you develop alone, in six months when reinstalling the project you can feel confident the dependencies installed are still working even if your dependencies released many new versions since then.

#### Updating Dependencies to their Latest Versions

* The composer.lock file prevents you from automatically getting the latest versions of your dependencies.

* To update to the latest versions, use the `update` command.

* This will fetch the latest matching versions (according to your `composer.json` file) and update the `composer.lock` file with the new versions.

```
composer update

```

* If you only want to `update` one dependency, you can _whitelist_ them:

```
composer update silber/bouncer
```

### NPM

* NPM is the dependency manager for JavaScript, just like Composer is the dependency manager for PHP.

* Modern web apps are never complete without any client-side scripting, hence, the use of NPM to manage JavaScript dependencies in our project is required.

#### Installing Packages

* To install:

```
npm install --save <package_name>
```

* This will create the `node_modules` directory in your current directory (if one doesn't exist yet) and will download the package to that directory.

* If there is no `package.json` file in the local directory, the latest version of the package is installed.

* If there is a `package.json` file, npm installs the latest version that satisfies the `semver` rule declared in `package.json`.

#### Using the Installed Package in Your Code

* Once the package is in `node_modules`, you can use it in your code:

```javascript
var lodash = require('lodash');
 
var output = lodash.without([1, 2, 3], 1);
console.log(output);
```

* If you had not properly installed `lodash`, you would receive this error:

```javascript
module.js:340
    throw err;
          ^
Error: Cannot find module 'lodash'
```

#### Working with `package.json`

* An example of `package.json` file is as follows:

```javascript
{
    "private": true,
    "scripts": {
        "dev": "npm run development",
        "development": "cross-env NODE_ENV=development node_modules/webpack/bin/webpack.js --progress --hide-modules --config=node_modules/laravel-mix/setup/webpack.config.js",
        "watch": "cross-env NODE_ENV=development node_modules/webpack/bin/webpack.js --watch --progress --hide-modules --config=node_modules/laravel-mix/setup/webpack.config.js",
        "watch-poll": "npm run watch -- --watch-poll",
        "hot": "cross-env NODE_ENV=development node_modules/webpack-dev-server/bin/webpack-dev-server.js --inline --hot --config=node_modules/laravel-mix/setup/webpack.config.js",
        "prod": "npm run production",
        "production": "cross-env NODE_ENV=production node_modules/webpack/bin/webpack.js --no-progress --hide-modules --config=node_modules/laravel-mix/setup/webpack.config.js"
    },
    "devDependencies": {
        "axios": "^0.17",
        "babel-preset-react": "^6.23.0",
        "bootstrap-sass": "^3.3.7",
        "cross-env": "^5.1",
        "jquery": "^3.2",
        "laravel-mix": "^1.0",
        "lodash": "^4.17.4",
        "react": "^15.4.2",
        "react-dom": "^15.4.2"
    },
    "dependencies": {
        "exif-js": "^2.3.0",
        "hashids": "^1.1.4",
        "moment": "^2.21.0",
        "react-file-drop": "^0.1.9",
        "react-image-lightbox": "^4.6.0",
        "react-lazyload": "^2.3.0",
        "react-modal": "^3.3.2",
        "remodal": "^1.1.1"
    }
}

```

* The version numbering follows semver specs just like Composer.

#### Updating Packages

* It's a good practice to periodically update the packages your application depends on.

* Then, if the original developers have improved their code, your code will be improved as well:

```
npm update
```

---

## Managing Assets

### JavaScript & CSS Scaffolding

* While Laravel does not dictate which JavaScript or CSS pre-processors you use, it does provide a basic starting point using *Bootstrap* and *Vue* that will be helpful for many applications.

* Laravel uses NPM to install both of these frontend packages.

#### CSS

* *Laravel Mix* provides a clean, expressive API over compiling SASS or Less, which are extensions of plain CSS that add variables, mixins, and other powerful features that make working with CSS much more enjoyable.


#### JavaScript

* Laravel does not require you to use a specific JavaScript framework or library to build your applications.

* However, Laravel does include some basic scaffolding to make it easier to get started writing modern JavaScript using the *Vue* library. As with CSS, we may use Laravel Mix to easily compile JavaScript components into a single, browser-ready JavaScript file.

#### Removing The Frontend Scaffolding

* If you would like to remove the frontend scaffolding from your application, you may use the `preset` Artisan command. This command, when combined with the `none` option, will remove the *Bootstrap* and *Vue* scaffolding from your application, leaving only a blank SASS file and a few common JavaScript utility libraries:

```
php artisan preset none
```

#### Writing CSS

* Laravel's `package.json` file includes the `bootstrap-sass` package to help you get started prototyping your application's frontend using Bootstrap.

* However, feel free to add or remove packages from the `package.json` file as needed for your own application. You are not required to use the Bootstrap framework to build your Laravel application - it is provided as a good starting point for those who choose to use it.

* Before compiling your CSS, install your project's frontend dependencies using NPM:

```
npm install
```

* Once the dependencies have been installed using `npm install`, you can compile your SASS files to plain CSS using Laravel Mix.

* The `npm run dev` command will process the instructions in your `webpack.mix.js` file.

* Typically, your compiled CSS will be placed in the `public/css` directory:

```
npm run dev
```

* The default `webpack.mix.js` included with Laravel will compile the `resources/assets/sass/app.scss` SASS file.

* This `app.scss` file imports a file of SASS variables and loads Bootstrap, which provides a good starting point for most applications.

#### Writing JavaScript

* All of the JavaScript dependencies required by your application can be found in the `package.json` file in the project's root directory.

* You can install these dependencies using NPM:

```
npm install
```

* By default, the Laravel `package.json` file includes a few packages such as `vue` and `axios` to help you get started building your JavaScript application.

* Feel free to add or remove from the `package.json` file as needed for your own application.

* Once the packages are installed, you can use the `npm run dev` command to compile your assets.

* By default, the Laravel `webpack.mix.js` file compiles your SASS and the `resources/assets/js/app.js` file.

* Within the app.js file you may register your Vue components or, if you prefer a different framework, configure your own JavaScript application.

* Your compiled JavaScript will typically be placed in the `public/js` directory.

* The `app.js` file will load the `resources/assets/js/bootstrap.js` file which bootstraps and configures Vue, Axios, jQuery, and all other JavaScript dependencies.

* If you have additional JavaScript dependencies to configure, you may do so in this file.

#### Using React

* If you prefer to use React to build your JavaScript application, Laravel makes it a cinch to swap the Vue scaffolding with React scaffolding.

* On any fresh Laravel application, you may use the `preset` command with the `react` option:

```
php artisan preset react
```

* This single command will remove the Vue scaffolding and replace it with React scaffolding, including an example component.

### Compiling Assets using Laravel Mix

* Laravel Mix provides a fluent API for defining Webpack build steps for your Laravel application using several common CSS and JavaScript pre-processors.

* Through simple method chaining, you can fluently define your asset pipeline:

```javascript
mix.js('resources/assets/js/app.js', 'public/js')
   .sass('resources/assets/sass/app.scss', 'public/css');
```

#### Installation & Setup

* Before triggering Mix, you must first ensure that Node.js and NPM are installed on your machine.

```
node -v
npm -v
```

* You can easily install the latest version of Node and NPM using simple graphical installers from their download page.

* The only remaining step is to install Laravel Mix.

* Within a fresh installation of Laravel, you'll find a `package.json` file in the root of your directory structure. The default `package.json` file includes everything you need to get started:

```
npm install
```

#### Running Mix

* Mix is a configuration layer on top of Webpack, so to run your Mix tasks you only need to execute one of the NPM scripts that is included with the default Laravel `package.json` file:

```
// Run all Mix tasks...
npm run dev
```

```
// Run all Mix tasks and minify output...
npm run production
```
##### Watching Assets For Changes

* The `npm run watch` command will continue running in your terminal and watch all relevant files for changes.

* Webpack will then automatically recompile your assets when it detects a change:

```
npm run watch
```

* You may find that in certain environments Webpack isn't updating when your files change.

* If this is the case on your system, consider using the `watch-poll` command:

```
npm run watch-poll
```

#### Working With Stylesheets

* The `webpack.mix.js` file is your entry point for all asset compilation.

* Mix tasks can be chained together to define exactly how your assets should be compiled.

##### Less

* The `less` method may be used to compile Less into CSS.

* Let's compile our primary `app.less` file to `public/css/app.css`:

```javascript
mix.less('resources/assets/less/app.less', 'public/css');
```

* Multiple calls to the `less` method may be used to compile multiple files:

```javascript
mix.less('resources/assets/less/app.less', 'public/css')
   .less('resources/assets/less/admin.less', 'public/css');
```

* If you wish to customize the file name of the compiled CSS, you may pass a full file path as the second argument to the less method:

```javascript
mix.less('resources/assets/less/app.less', 'public/stylesheets/styles.css');
```

* If you need to override the underlying Less plug-in options, you may pass an object as the third argument to `mix.less()`:

```javascript
mix.less('resources/assets/less/app.less', 'public/css', {
    strictMath: true
});
```

##### Sass

* The `sass` method allows you to compile Sass into CSS:

```javascript
mix.sass('resources/assets/sass/app.scss', 'public/css');
```

* Again, like the `less` method, you may compile multiple Sass files into their own respective CSS files and even customize the output directory of the resulting CSS:

```javascript
mix.sass('resources/assets/sass/app.sass', 'public/css')
   .sass('resources/assets/sass/admin.sass', 'public/css/admin');
```

* Additional Node-Sass plug-in options may be provided as the third argument:

```javascript
mix.sass('resources/assets/sass/app.sass', 'public/css', {
    precision: 5
});
```

##### Stylus

* Similar to Less and Sass, the `stylus` method allows you to compile Stylus into CSS:

```javascript
mix.stylus('resources/assets/stylus/app.styl', 'public/css');
```

* You may also install additional Stylus plug-ins, such as Rupture.

* First, install the plug-in in question through NPM (`npm install rupture`) and then require it in your call to `mix.stylus()`:

```javascript
mix.stylus('resources/assets/stylus/app.styl', 'public/css', {
    use: [
        require('rupture')()
    ]
});
```

##### PostCSS

* PostCSS, a powerful tool for transforming your CSS, is included with Laravel Mix out of the box.

* By default, Mix leverages the popular Autoprefixer plug-in to automatically apply all necessary CSS3 vendor prefixes. 

* However, you're free to add any additional plug-ins that are appropriate for your application.

* First, install the desired plug-in through NPM and then reference it in your `webpack.mix.js` file:

```javascript
mix.sass('resources/assets/sass/app.scss', 'public/css')
   .options({
        postCss: [
            require('postcss-css-variables')()
        ]
   });
```

##### Plain CSS

* If you would just like to concatenate some plain CSS stylesheets into a single file, you may use the `styles` method.

```javascript
mix.styles([
    'public/css/vendor/normalize.css',
    'public/css/vendor/videojs.css'
], 'public/css/all.css');
```

#### URL Processing

* Because Laravel Mix is built on top of Webpack, it's important to understand a few Webpack concepts.

* For CSS compilation, Webpack will rewrite and optimize any `url()` calls within your stylesheets.

* While this might initially sound strange, it's an incredibly powerful piece of functionality.

* Imagine that we want to compile Sass that includes a relative URL to an image:

```css
.example {
    background: url('../images/example.png');
}
```

* Absolute paths for any given `url()` will be excluded from URL-rewriting.

* For example, `url('/images/thing.png')` or  `url('http://example.com/images/thing.png')` won't be modified.

* By default, Laravel Mix and Webpack will find `example.png`, copy it to your `public/image`s folder, and then rewrite the `url()` within your generated stylesheet.

* As such, your compiled CSS will be:

```css
.example {
  background: url(/images/example.png?d41d8cd98f00b204e9800998ecf8427e);
}
```

* As useful as this feature may be, it's possible that your existing folder structure is already configured in a way you like.

* If this is the case, you may disable `url()` rewriting:

```javascript
mix.sass('resources/assets/app/app.scss', 'public/css')
   .options({
      processCssUrls: false
   });
```

* With this addition to your webpack.mix.js file, Mix will no longer match any `url()` or copy assets to your public directory.

* In other words, the compiled CSS will look just like how you originally typed it:

```css
.example {
    background: url("../images/thing.png");
}
```

#### Working With JavaScript

* Mix provides several features to help you work with your JavaScript files, such as compiling ECMAScript 2015, module bundling, minification, and concatenating plain JavaScript files

* Even better, this all works seamlessly, without requiring an ounce of custom configuration:

```javascript
mix.js('resources/assets/js/app.js', 'public/js');
```

##### Vendor Extraction

* One potential downside to bundling all application-specific JavaScript with your vendor libraries is that it makes long-term caching more difficult.

* A single update to your application code will force the browser to re-download all of your vendor libraries even if they haven't changed.

* If you intend to make frequent updates to your application's JavaScript, you should consider extracting all of your vendor libraries into their own file.

* This way, a change to your application code will not affect the caching of your large `vendor.js` file.

* Mix's `extract` method makes this a breeze:

```javascript
mix.js('resources/assets/js/app.js', 'public/js')
   .extract(['vue']);
```

```javascript
mix.react('resources/assets/js/app.js', 'public/js')
   .extract(['react', 'react-dom']);
```

* The `extract` method accepts an array of all libraries or modules that you wish to extract into a `vendor.js` file.

* Using the above examples, Mix will generate the following files:

  * `public/js/manifest.js`: The Webpack manifest runtime

  * `public/js/vendor.js`: Your vendor libraries

  * `public/js/app.js`: Your application code
  
* To avoid JavaScript errors, be sure to load these files in the proper order:

```php
<script src="{{asset('js/manifest.js')}}"></script>
<script src="{{asset('js/vendor.js')}}"></script>
<script src="{{asset('js/app.js')}}"></script>
```

##### React

* Mix can automatically install the Babel plug-ins necessary for React support.

* To get started, replace your `mix.js()` call with `mix.react()`:

```javascript
mix.react('resources/assets/js/app.jsx', 'public/js');
```

* Behind the scenes, Mix will download and include the appropriate babel-preset-react Babel plug-in.


##### Vanilla JS/jQuery

* Similar to combining stylesheets with `mix.styles()`, you may also combine and minify any number of JavaScript files with the scripts() method:

```javascript
mix.scripts([
    'public/js/admin.js',
    'public/js/dashboard.js'
], 'public/js/all.js');
```

* This option is particularly useful for legacy projects where you don't require Webpack compilation for your JavaScript.

#### Notifications

* When available, Mix will automatically display OS notifications for each bundle.

* This will give you instant feedback, as to whether the compilation was successful or not.

* However, there may be instances when you'd prefer to disable these notifications.

* One such example might be triggering Mix on your production server.

* Notifications may be deactivated, via the `disableNotifications` method:

```javascript
mix.disableNotifications();
```
---

## Data Migration

* We have used the migration tool in Laravel to create our database with the required tables, as well as to automatically insert default rows in some tables.

* Besides that, we can also use the migration tool to write the required script to migrate data from an existing database to the new one, if required.

---
###### The contents in this page are generally based on Laravel's Documentation at https://laravel.com/docs/5.5,  Composer at https://getcomposer.org/doc/ and NPM at https://docs.npmjs.com/.

</xmp>
<script type="text/javascript" src="https://cdn.rawgit.com/Naereen/StrapDown.js/master/strapdown.min.js?mathjax=y&src=example5&theme=united"></script>
</body>
</html>
