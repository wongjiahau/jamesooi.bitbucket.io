<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>Lecture 06: Authentication &amp; Authorization</title>
</head>
<body>
<xmp>

# Lecture 06: Authentication & Authorization

---

## Introduction

* Laravel makes implementing authentication very simple as almost everything is configured out of the box.

* The authentication configuration file is located at  `config/auth.php`, which contains several well documented options for tweaking the behavior of the authentication services.

* At its core, Laravel's authentication facilities are made up of _guards_ and _providers_.

### Guards

* Guards define how users are authenticated for each request. 

* For example, Laravel ships with a `session` guard which maintains state using session storage and cookies.

### Providers

* Providers define how users are retrieved from your persistent storage.

* Laravel ships with support for retrieving users using Eloquent and the database query builder.

* However, you are free to define additional providers as needed for your application.

---

## Database Considerations

* By default, Laravel includes an `App\User` Eloquent model in your `app` directory.

* This model may be used with the default Eloquent authentication driver.

* If your application is not using Eloquent, you may use the `database` authentication driver which uses the Laravel query builder.

* When building the database schema for the `App\User` model, make sure the `password` column is at least 60 characters in length.

* Verify that your `users` (or equivalent) table contains a nullable, string `remember_token` column of 100 characters. This column will be used to store a token for users that select the _remember me_ option when logging into your application.

---

## Authentication Quickstart

* Laravel ships with several pre-built authentication controllers, which are located in the  `App\Http\Controllers\Auth` namespace.

  * The `RegisterController` handles new user registration.
  
  * The `LoginController` handles authentication.
  
  * The `ForgotPasswordController` handles e-mailing links for resetting passwords.
  
  * The `ResetPasswordController` contains the logic to reset passwords.
  
* Each of these controllers uses a trait to include their necessary methods.

* For many applications, you will not need to modify these controllers at all.


### Routing

* Laravel provides a quick way to scaffold all of the routes and views you need for authentication using one simple command:

```php
php artisan make:auth
```

* This command should be used on fresh applications and will install a layout view, registration and login views, as well as routes for all authentication end-points.

* A `HomeController` will also be generated to handle post-login requests to your application's dashboard.


### Views

* The `make:auth` Artisan command will create all of the views you need for authentication and place them in the `resources/views/auth` directory.

* The `make:auth` command will also create a `resources/views/layouts` directory containing a base layout for your application. All of these views use the Bootstrap CSS framework, but you are free to customize them however you wish.


### Authenticating

* Now that you have routes and views setup for the included authentication controllers, you are ready to register and authenticate new users for your application!

* You may access your application in a browser since the authentication controllers already contain the logic (via their traits) to authenticate existing users and store new users in the database.


### Path Customization

* When a user is successfully authenticated, by default, they will be redirected to the `/home` URI.

* You can customize the post-authentication redirect location by defining a `redirectTo` property on the  `LoginController`, `RegisterController`, and `ResetPasswordController`:

```php
protected $redirectTo = '/';
```

* If the redirect path needs custom generation logic you may define a `redirectTo` method instead of a `redirectTo` property:

```php
protected function redirectTo()
{
    return '/path';
}
```

* The `redirectTo` method will take precedence over the `redirectTo` attribute.

### Username Customization

* By default, Laravel uses the `email` field for authentication.

* If you would like to customize this, you may define a `username` method on your `LoginController`:

```php
public function username()
{
    return 'username';
}
```

### Guard Customization

* You may also customize the _guard_ that is used to authenticate and register users.

* To get started, define a `guard` method on your `LoginController`, `RegisterController`, and  `ResetPasswordController`. The method should return a guard instance:

```php
use Illuminate\Support\Facades\Auth;

protected function guard()
{
    return Auth::guard('guard-name');
}
```

### Validation/Storage Customization

* To modify the form fields that are required when a new user registers with your application, or to customize how new users are stored into your database, you may modify the `RegisterController` class. This class is responsible for validating and creating new users of your application.

* The `validator` method of the `RegisterController` contains the validation rules for new users of the application. You are free to modify this method as you wish.

* The `create` method of the `RegisterController` is responsible for creating new `App\User` records in your database using the Eloquent ORM. You are free to modify this method according to the needs of your database.

### Retrieving The Authenticated User

* You may access the authenticated user via the `Auth` facade:

```php
use Illuminate\Support\Facades\Auth;

// Get the currently authenticated user...
$user = Auth::user();

// Get the currently authenticated user's ID...
$id = Auth::id();
```

* Alternatively, once a user is authenticated, you may access the authenticated user via an  `Illuminate\Http\Request` instance.

* Remember, type-hinted classes will automatically be injected into your controller methods:

```php
namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ProfileController extends Controller
{
    /**
     * Update the user's profile.
     *
     * @param  Request  $request
     * @return Response
     */
    public function update(Request $request)
    {
        // $request->user() returns an instance of the authenticated user...
    }
}
```

### Determining If The Current User Is Authenticated

* To determine if the user is already logged into your application, you may use the `check` method on the `Auth` facade, which will return `true` if the user is authenticated:

```php
use Illuminate\Support\Facades\Auth;

if (Auth::check()) {
    // The user is logged in...
}
```

### Protecting Routes

* Even though it is possible to determine if a user is authenticated using the `check` method, you will typically use a _middleware_ to verify that the user is authenticated before allowing the user access to certain routes / controllers. 

* Route middleware can be used to only allow authenticated users to access a given route.

* Laravel ships with an `auth` middleware, which is defined at `Illuminate\Auth\Middleware\Authenticate`.

* Since this middleware is already registered in your HTTP kernel, all you need to do is attach the middleware to a route definition:

```php
Route::get('profile', function () {
    // Only authenticated users may enter...
})->middleware('auth');
```

* Of course, if you are using controllers, you may call the `middleware` method from the controller's constructor instead of attaching it in the route definition directly:

```php
public function __construct()
{
    $this->middleware('auth');
}
```

### Specifying A Guard

* When attaching the auth middleware to a route, you may also specify which guard should be used to authenticate the user.

* The guard specified should correspond to one of the keys in the `guards` array of your `auth.php` configuration file:

```php
public function __construct()
{
    $this->middleware('auth:api');
}
```

### Login Throttling

* If you are using Laravel's built-in `LoginController` class, the `Illuminate\Foundation\Auth\ThrottlesLogins` trait will already be included in your controller.

* By default, the user will not be able to login for one minute if they fail to provide the correct credentials after several attempts.

* The throttling is unique to the user's `username` / `e-mail` address and their IP address.


### Manually Authenticating Users

* Other than using the authentication controllers included with Laravel, you may choose to remove these controllers and manage user authentication using the Laravel authentication classes directly.

* We will access Laravel's authentication services via the `Auth` facade, so we'll need to make sure to import the `Auth` facade at the top of the class.

* Use the `attempt` method:

```php
namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    /**
     * Handle an authentication attempt.
     *
     * @return Response
     */
    public function authenticate()
    {
        if (Auth::attempt(['email' => $email, 'password' => $password])) {
            // Authentication passed...
            return redirect()->intended('dashboard');
        }
    }
}
```

* The `attempt` method accepts an array of key / value pairs as its first argument.

  * You should not hash the `password` specified as the password value, since the framework will automatically hash the value before comparing it to the hashed password in the database.

* The `attempt` method will return `true` if authentication was successful. Otherwise, `false` will be returned.

* The `intended` method on the redirector will redirect the user to the URL they were attempting to access before being intercepted by the authentication middleware. 

#### Specifying Additional Conditions

* You may also add extra conditions to the authentication query in addition to the user's `email` and `password`. For example, we may verify that user is marked as _active_:

```php
if (Auth::attempt(['email' => $email, 'password' => $password, 'active' => 1])) {
    // The user is active, not suspended, and exists.
}
```

#### Logging Out

* To log users out of your application, you may use the `logout` method on the `Auth` facade. This will clear the authentication information in the user's session:

```php
Auth::logout();
```

#### Remembering Users

* If you would like to provide _remember me_ functionality in your application, you may pass a `boolean` value as the second argument to the `attempt method`, which will keep the user authenticated indefinitely, or until they manually logout.

* The `users` table must include the string `remember_token` column, which will be used to store the _remember me_ token.

```php
if (Auth::attempt(['email' => $email, 'password' => $password], $remember)) {
    // The user is being remembered...
}
```

---

## Authorization

* Laravel provides a simple way to authorize user actions against a given resource. Like authentication.

* There are two primary ways of authorizing actions: _gates_ and _policies_.

  * Gates provide a simple, `Closure` based approach to authorization.
  
  * Policies group their logic around a particular model or resource.

* You do not need to choose between exclusively using gates or exclusively using policies when building an application.

* Most applications will most likely contain a mixture of gates and policies.

* Gates are most applicable to actions which are not related to any model or resource, such as viewing an administrator dashboard.

* Policies should be used when you wish to authorize an action for a particular model or resource.


### Gates

#### Writing Gates

* Gates are `Closures` that determine if a user is authorized to perform a given action and are typically defined in the `App\Providers\AuthServiceProvider` class using the `Gate` facade.

* Gates always receive a `User` instance as their first argument, and may optionally receive additional arguments such as a relevant Eloquent model:

```php
/**
 * Register any authentication / authorization services.
 *
 * @return void
 */
public function boot()
{
    $this->registerPolicies();

    Gate::define('update-post', function ($user, $post) {
        return $user->id == $post->user_id;
    });
}
```

* Gates may also be defined using a `Class@method` style callback string, like controllers:

```php
/**
 * Register any authentication / authorization services.
 *
 * @return void
 */
public function boot()
{
    $this->registerPolicies();

    Gate::define('update-post', 'PostPolicy@update');
}
```

#### Resource Gates

* You may also define multiple `Gate` abilities at once using the `resource` method:

```php
Gate::resource('posts', 'PostPolicy');
```

* This is identical to manually defining the following `Gate` definitions:

#### Authorizing Actions

* To authorize an action using gates, you should use the `allows` or `denies` methods. 

  * You do not need to pass the currently authenticated user to these methods. Laravel will automatically take care of passing the `User` into the gate `Closure`:

```php
if (Gate::allows('update-post', $post)) {
    // The current user can update the post...
}

if (Gate::denies('update-post', $post)) {
    // The current user can't update the post...
}
```

* If you would like to determine if a particular user is authorized to perform an action, you may use the `forUser` method on the `Gate` facade:

```php
if (Gate::forUser($user)->allows('update-post', $post)) {
    // The user can update the post...
}

if (Gate::forUser($user)->denies('update-post', $post)) {
    // The user can't update the post...
}
```

### Policies

#### Generating Policies

* Policies are classes that organize authorization logic around a particular model or resource.

  * E.g., if your application is a blog, you may have a `Post` model and a corresponding `PostPolicy` to authorize user actions such as creating or updating posts.

* You may generate a policy using the `make:policy` Artisan command.

* The generated policy will be placed in the `app/Policies` directory:

```
php artisan make:policy PostPolicy
```

* The `make:policy` command will generate an empty policy class.

* If you would like to generate a class with the basic CRUD policy methods already included in the class, you may specify a `--model` when executing the command:

```
php artisan make:policy PostPolicy --model=Post
```

* All policies are resolved via the Laravel service container, allowing you to type-hint any needed dependencies in the policy's constructor to have them automatically injected.

#### Registering Policies

* Once the policy exists, it needs to be registered.

* The `AuthServiceProvider` included with fresh Laravel applications contains a `policies` property which maps your Eloquent models to their corresponding policies. 

* Registering a policy will instruct Laravel which policy to utilize when authorizing actions against a given model:

```php
namespace App\Providers;

use App\Post;
use App\Policies\PostPolicy;
use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        Post::class => PostPolicy::class,
    ];

    /**
     * Register any application authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        //
    }
}
```

#### Policy Methods

* Once the policy has been registered, you may add methods for each action it authorizes.

* Let's define an `update` method on our `PostPolicy` which determines if a given `User` can update a given `Post` instance.

  * The `update` method will receive a `User` and a `Post` instance as its arguments, and should return `true` or `false` indicating whether the user is authorized to update the given `Post`:

```php
namespace App\Policies;

use App\User;
use App\Post;

class PostPolicy
{
    /**
     * Determine if the given post can be updated by the user.
     *
     * @param  \App\User  $user
     * @param  \App\Post  $post
     * @return bool
     */
    public function update(User $user, Post $post)
    {
        return $user->id === $post->user_id;
    }
}
```

* You may continue to define additional methods on the policy such as `view` or `delete` methods to authorize various `Post` actions

* Do remember however you are free to give your policy methods any name you like!

#### Methods Without Models

* Some policy methods only receive the currently authenticated user and not an instance of the model they authorize.

* This situation is most common when authorizing `create` actions.

  * E.g.: if you are creating a blog, you may wish to check if a user is authorized to create any posts at all.

* When defining policy methods that will not receive a model instance, such as a `create` method, it will not receive a model instance.

* Instead, you should define the method as only expecting the authenticated user:

```php
/**
 * Determine if the given user can create posts.
 *
 * @param  \App\User  $user
 * @return bool
 */
public function create(User $user)
{
    //
}
```

#### Policy Filters

* For certain users, you may wish to authorize all actions within a given policy.

* To accomplish this, define a `before` method on the policy.

  * The `before` method will be executed before any other methods on the policy, giving you an opportunity to authorize the action before the intended policy method is actually called.
  
  * This feature is most commonly used for authorizing application administrators to perform any action:

```php
public function before($user, $ability)
{
    if ($user->isSuperAdmin()) {
        return true;
    }
}
```

#### Authorizing Actions Using Policies via The User Model

* The User model that is included with your Laravel application includes two helpful methods for authorizing actions: `can` and `cant`.

* The `can` method receives the action you wish to authorize and the relevant model. 

  * E.g., let's determine if a user is authorized to update a given `Post` model:

```php
if ($user->can('update', $post)) {
    //
}
```

#### Actions That Don't Require Models

* In these situations, you may pass a class name to the `can` method.

  * The class name will be used to determine which policy to use when authorizing the action:

```php
use App\Post;

if ($user->can('create', Post::class)) {
    // Executes the "create" method on the relevant policy...
}
```

#### Authorizing Actions Using Policies via Middleware
 
* Laravel includes a middleware that can authorize actions before the incoming request even reaches your routes or controllers.

* By default, the `Illuminate\Auth\Middleware\Authorize` middleware is assigned the `can` key in your `App\Http\Kernel` class:

```php
use App\Post;

Route::put('/post/{post}', function (Post $post) {
    // The current user may update the post...
})->middleware('can:update,post');
```

* In this example, we're passing the `can` middleware two arguments.

  * The first is the name of the action we wish to authorize
  
  * The second is the route parameter we wish to pass to the policy method.
  
* In this case, since we are using implicit model binding, a `Post` model will be passed to the policy method.

* If the user is not authorized to perform the given action, a HTTP response with a 403 status code will be generated by the middleware.

#### Actions That Don't Require Models

* In these situations, you may pass a class name to the middleware. The class name will be used to determine which policy to use when authorizing the action:

```php
Route::post('/post', function () {
    // The current user may create posts...
})->middleware('can:create,App\Post');
```

#### Authorizing Actions Using Policies via Controller Helpers

* Laravel provides a helpful `authorize` method to any of your controllers which extend the `App\Http\Controllers\Controller` base class.

* Like the `can` method, this method accepts the name of the action you wish to authorize and the relevant model.

* If the action is not authorized, the `authorize` method will throw an `Illuminate\Auth\Access\AuthorizationException`, which the default Laravel exception handler will convert to an HTTP response with a 403 status code:

```php
namespace App\Http\Controllers;

use App\Post;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PostController extends Controller
{
    /**
     * Update the given blog post.
     *
     * @param  Request  $request
     * @param  Post  $post
     * @return Response
     */
    public function update(Request $request, Post $post)
    {
        $this->authorize('update', $post);

        // The current user can update the blog post...
    }
}
```

#### Actions That Don't Require Models

* In these situations, you pass a class name to the authorize method.

* The class name will be used to determine which policy to use when authorizing the action:

```php
/**
 * Create a new blog post.
 *
 * @param  Request  $request
 * @return Response
 */
public function create(Request $request)
{
    $this->authorize('create', Post::class);

    // The current user can create blog posts...
}
```

#### Authorizing Actions Using Policies via Blade Templates

* When writing Blade templates, you may wish to display a portion of the page only if the user is authorized to perform a given action.

  * E.g., you may wish to show an update form for a blog post only if the user can actually update the post.
  
* In this situation, you may use the `@can` and `@cannot` family of directives:

```php
@can('update', $post)
    <!-- The Current User Can Update The Post -->
@elsecan('create', App\Post::class)
    <!-- The Current User Can Create New Post -->
@endcan

@cannot('update', $post)
    <!-- The Current User Can't Update The Post -->
@elsecannot('create', App\Post::class)
    <!-- The Current User Can't Create New Post -->
@endcannot
```

* These directives are convenient shortcuts for writing `@if` and `@unless` statements. 

* The `@can` and `@cannot` statements above respectively translate to the following statements:

```php
@if (Auth::user()->can('update', $post))
    <!-- The Current User Can Update The Post -->
@endif

@unless (Auth::user()->can('update', $post))
    <!-- The Current User Can't Update The Post -->
@endunless
```

#### Actions That Don't Require Models

* Like most of the other authorization methods, you may pass a class name to the `@can` and `@cannot` directives if the action does not require a model instance:

```php
@can('create', App\Post::class)
    <!-- The Current User Can Create Posts -->
@endcan

@cannot('create', App\Post::class)
    <!-- The Current User Can't Create Posts -->
@endcannot
```

---
###### The contents in this page are generally based on Laravel's Documentation at https://laravel.com/docs/5.5
</xmp>
<script type="text/javascript" src="https://cdn.rawgit.com/Naereen/StrapDown.js/master/strapdown.min.js?mathjax=y&src=example5&theme=united"></script>
</body>
</html>
