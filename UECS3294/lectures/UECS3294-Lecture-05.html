<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>Lecture 05: Maintaining State</title>
</head>
<body>
<xmp>

# Lecture 05: Maintaining State

---

## Cookies

* All cookies created by the Laravel framework are encrypted and signed with an authentication code, meaning they will be considered invalid if they have been changed by the client.

### Retrieving Cookies From Requests

* Use the `cookie` method on a `Illuminate\Http\Request` instance to retrieve a cookie value from the request:

```php
$value = $request->cookie('name');
```

* Alternatively, use the `Cookie` facade:

```php
$value = Cookie::get('name');
```

### Attaching Cookies To Responses

* The `cookie` method on response instances allows you to easily attach cookies to the response:

```php
return response($content)
    ->header('Content-Type', $type)
    ->cookie('name', 'value', $expire_in_minutes);
```

* With additional arguments:

```php
return response($content)
    ->header('Content-Type', $type)
    ->cookie('name', 'value', $expire_in_minutes, $path, $domain, $secure, $httpOnly);
```

* Alternatively, use the `Cookie` facade to queue cookies for attachment to the outgoing response from your application.

* The `queue` method accepts a `Cookie` instance or the arguments needed to create a `Cookie` instance.

* These cookies will be attached to the outgoing response before it is sent to the browser:

```php
Cookie::queue(Cookie::make('name', 'value', $minutes));

// OR

Cookie::queue('name', 'value', $minutes);
```

---

## Session

* Since HTTP driven applications are stateless, sessions provide a way to store information about the user across multiple requests.

* Laravel ships with a variety of session backends that are accessed through an expressive, unified API.

* Support for popular backends such as **Memcached**, **Redis**, and databases is included out of the box.

### Configuration

* The session configuration file is stored at `config/session.php`.

* By default, Laravel is configured to use the `file` session driver, which will work well for many applications.

* In production applications, you may consider using the `memcached` or `redis` drivers for even faster session performance.

* The session driver configuration option defines where session data will be stored for each request.

* Laravel ships with several drivers out of the box:

  * `file` - sessions are stored in `storage/framework/sessions`.

  * `cookie` - sessions are stored in secure, encrypted cookies.

  * `database` - sessions are stored in a relational database.

  * `memcached` / `redis` - sessions are stored in one of these fast, cache based stores.

  * `array` - sessions are stored in a PHP array and will not be persisted.
  
### Retrieving Session Data

* There are two primary ways of working with session data in Laravel:

  * the global `session` helper
  
  * via a Request instance by type-hinting in a controller method:

#### Using Request Instance
  
```php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UserController extends Controller
{
    /**
     * Show the profile for the given user.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function show(Request $request, $id)
    {
        $value = $request->session()->get('key');

        //
    }
}
```

* When you retrieve a value from the session, you may also pass a default value as the second argument to the `get` method, or a `Closure` as the default value to the `get` method:

```php
$value = $request->session()->get('key', 'default');

$value = $request->session()->get('key', function () {
    return 'default';
});
```

#### Using Global Session Helper

* You may also use the global `session` PHP function to retrieve and store data in the session.

* When the `session` helper is called with a single, string argument, it will return the value of that session key.

* When the helper is called with an array of key/value pairs, those values will be stored in the session:

```php
Route::get('home', function () {
    // Retrieve a piece of data from the session...
    $value = session('key');

    // Specifying a default value...
    $value = session('key', 'default');

    // Store a piece of data in the session...
    session(['key' => 'value']);
});
```

#### Retrieving All Session Data

* If you would like to retrieve all the data in the session, you may use the `all` method:

```php
$data = $request->session()->all();
```

#### Determining If An Item Exists In The Session

* To determine if a value is present in the session, you may use the `has` method.'

* The `has` method returns `true` if the value is present and is not `null`:

```php
if ($request->session()->has('users')) {
    //
}
```

* To determine if a value is present in the session, even if its value is `null`, you may use the `exists` method.

* The `exists` method returns `true` if the value is present:

```php
if ($request->session()->exists('users')) {
    //
}
```

### Storing Data in Session

* To store data in the session, you will typically use the `put` method or the `session` helper:

```php
// Via a request instance...
$request->session()->put('key', 'value');

// Via the global helper...
session(['key' => 'value']);
```

#### Pushing To Array Session Values

* The `push` method may be used to push a new value onto a session value that is an array.

* For example, if the `user.teams` key contains an array of team names, you may push a new value onto the array:

```php
$request->session()->push('user.teams', 'developers');
```

#### Retrieving & Deleting An Item

* The `pull` method will retrieve and delete an item from the session in a single statement:

```php
$value = $request->session()->pull('key', 'default');
```

### Flash Data

* Sometimes you may wish to store items in the session only for the next request.

* You may do so using the `flash` method.

* Data stored in the session using this method will only be available during the subsequent HTTP request, and then will be deleted.

* Flash data is primarily useful for short-lived status messages:

```php
$request->session()->flash('status', 'Task was successful!');
```

* If you need to keep your flash data around for several requests, you may use the `reflash` method, which will keep all of the flash data for an additional request.

* If you only need to keep specific flash data, you may use the `keep` method:

```php
$request->session()->reflash();

$request->session()->keep(['username', 'email']);
```

### Deleting Data

* The `forget` method will remove a piece of data from the session. If you would like to remove all data from the session, you may use the `flush` method:

```php
$request->session()->forget('key');

$request->session()->flush();
```
---

## Keeping Old Input using Flash Data

* You can keep input from one request to be used during the next request.

* This feature is also known as **flash** and is particularly useful for re-populating forms after detecting validation errors. 

  * You are unlikely need to do so if you are using Laravel's validation features as some of Laravel's built-in validation facilities will call them automatically.

* This feature uses PHP _Session_.

### Flashing Input To The Session

* Flash the current input to the session so that it is available during the user's next request:

```php
$request->flash();
```

* Only flash the specified input fields:

```php
$request->flashOnly(['username', 'email']);
```

* Flash all input except the specified fields:

```php
$request->flashExcept('password');
```

### Flashing Input Then Redirecting

* Very often, you want to flash input to the session and then redirect to the previous page

* Chain input flashing onto a `redirect` using the `withInput` method:

```php
return redirect('form')->withInput();
```

```php
return redirect('form')->withInput(
    $request->except('password')
);
```

### Retrieving Old Input

* The `old` method of `Illuminate\Http\Request` will pull the previously flashed input data from the session:

```php
$username = $request->old('username');
```

* Use the global `old` helper function within a Blade template to display old input value. If no old input exists for the given field, `null` will be returned:

```php
<input type="text" name="username" value="{{ old('username') }}">
```

#### Redirecting With Flashed Session Data

* Redirecting to a new URL and flashing data to the session are usually done at the same time.

* Typically, this is done after successfully performing an action when you flash a success message to the session:

```php
Route::post('user/profile', function () {
    // Update the user's profile...

    return redirect('dashboard')->with('status', 'Profile updated!');
});
```

* After the user is redirected, you may display the flashed message from the session:

```php
@if (session('status'))
    <div class="alert alert-success">
        {{ session('status') }}
    </div>
@endif
```

###### The contents in this page are generally based on Laravel's Documentation at https://laravel.com/docs/5.5
</xmp> 
<script type="text/javascript" src="https://cdn.rawgit.com/Naereen/StrapDown.js/master/strapdown.min.js?mathjax=y&src=example5&theme=united"></script>
</body>
</html>
